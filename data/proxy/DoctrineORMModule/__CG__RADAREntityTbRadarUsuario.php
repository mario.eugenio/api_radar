<?php

namespace DoctrineORMModule\Proxy\__CG__\RADAR\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class TbRadarUsuario extends \RADAR\Entity\TbRadarUsuario implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'coUsuario', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'noUsuario', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'telUsuario', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'perfil', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'emailUsuario', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'senha', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'repitaSenha', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'stAtivo', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'imagemUsuario'];
        }

        return ['__isInitialized__', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'coUsuario', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'noUsuario', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'telUsuario', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'perfil', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'emailUsuario', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'senha', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'repitaSenha', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'stAtivo', '' . "\0" . 'RADAR\\Entity\\TbRadarUsuario' . "\0" . 'imagemUsuario'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (TbRadarUsuario $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getCoUsuario()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getCoUsuario();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCoUsuario', []);

        return parent::getCoUsuario();
    }

    /**
     * {@inheritDoc}
     */
    public function setCoUsuario($coUsuario)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCoUsuario', [$coUsuario]);

        return parent::setCoUsuario($coUsuario);
    }

    /**
     * {@inheritDoc}
     */
    public function getNoUsuario()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNoUsuario', []);

        return parent::getNoUsuario();
    }

    /**
     * {@inheritDoc}
     */
    public function setNoUsuario($noUsuario)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNoUsuario', [$noUsuario]);

        return parent::setNoUsuario($noUsuario);
    }

    /**
     * {@inheritDoc}
     */
    public function getTelUsuario()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTelUsuario', []);

        return parent::getTelUsuario();
    }

    /**
     * {@inheritDoc}
     */
    public function setTelUsuario($telUsuario)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTelUsuario', [$telUsuario]);

        return parent::setTelUsuario($telUsuario);
    }

    /**
     * {@inheritDoc}
     */
    public function getPerfil()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPerfil', []);

        return parent::getPerfil();
    }

    /**
     * {@inheritDoc}
     */
    public function setPerfil($perfil)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPerfil', [$perfil]);

        return parent::setPerfil($perfil);
    }

    /**
     * {@inheritDoc}
     */
    public function getEmailUsuario()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmailUsuario', []);

        return parent::getEmailUsuario();
    }

    /**
     * {@inheritDoc}
     */
    public function setEmailUsuario($emailUsuario)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmailUsuario', [$emailUsuario]);

        return parent::setEmailUsuario($emailUsuario);
    }

    /**
     * {@inheritDoc}
     */
    public function getSenha()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSenha', []);

        return parent::getSenha();
    }

    /**
     * {@inheritDoc}
     */
    public function setSenha($senha)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSenha', [$senha]);

        return parent::setSenha($senha);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepitaSenha()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRepitaSenha', []);

        return parent::getRepitaSenha();
    }

    /**
     * {@inheritDoc}
     */
    public function setRepitaSenha($repitaSenha)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRepitaSenha', [$repitaSenha]);

        return parent::setRepitaSenha($repitaSenha);
    }

    /**
     * {@inheritDoc}
     */
    public function getStAtivo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStAtivo', []);

        return parent::getStAtivo();
    }

    /**
     * {@inheritDoc}
     */
    public function setStAtivo($stAtivo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStAtivo', [$stAtivo]);

        return parent::setStAtivo($stAtivo);
    }

    /**
     * {@inheritDoc}
     */
    public function getImagemUsuario()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImagemUsuario', []);

        return parent::getImagemUsuario();
    }

    /**
     * {@inheritDoc}
     */
    public function setImagemUsuario($imagemUsuario)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setImagemUsuario', [$imagemUsuario]);

        return parent::setImagemUsuario($imagemUsuario);
    }

    /**
     * {@inheritDoc}
     */
    public function setData($data)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setData', [$data]);

        return parent::setData($data);
    }

    /**
     * {@inheritDoc}
     */
    public function toArray()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'toArray', []);

        return parent::toArray();
    }

}
