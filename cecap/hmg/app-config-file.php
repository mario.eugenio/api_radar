<?php
ini_set('error_reporting', E_ALL);
ini_set('display_startup_errors', true);
ini_set('display_errors', true);

return[
    'notificacao' => [
      'mobile_schema' => 'DBMOBILE',
      'main_schema' => 'DBMOBILE',
      'version' => '1.0.0',
      'disable_project' => true, // habilita/desabilita função de desativar app's
      'db_adapter' => 'mec_mobile_adapter',
      'page_title' => 'Radar',
      'sigla' => 'radar',
      'copy_right' => '© Copyright ' . date('Y') . ' Ministério da Educação',
      'project' => 'Radar',
    ],
    'one_singnal' => [
      'options' => [
        'autorization_key' => '',
        'app_id' => '',
        'url_methods' => [
          'notification' => 'https://onesignal.com/api/v1/notifications'
        ]
      ]
    ],
    'db' => [
      'adapters' => [
        'mec_mobile_adapter' => [
          'database' => 'dsvora',
          'driver' => 'Oci8',
          'connection' => '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)
                  (HOST = DSV-ORACLE-SCAN)(PORT = 1521))(CONNECT_DATA =
                  (SERVER = DEDICATED)(SERVICE_NAME = DSVORA)))',
          'hostname' => 'examec01-scan4.mec.gov.br',
          'username' => 'DBWSRADAR',
          'password' => 'DBWSRADAR',
          'port' => '1521',
          'charset' => 'utf8',
          'platform_options' => array('quote_identifiers' => false)
        ]
      ],
    ],
    'caches' =>[
        /*'memcached' =>[
            'adapter' =>[
                'name'     =>'memcached',
                'minTtl' => 43200,
                'options'  =>[
                    'namespace' => 'WSEPROINFOLOCAL',
                    'ttl' => 43200,
                    'servers'   =>[
                        [
                            '10.37.0.34', 11216
                        ]
                    ],
                    'liboptions' => [
                        'COMPRESSION' => true,
                        'binary_protocol' => true,
                        'no_block' => true,
                        'connect_timeout' => 100
                    ]
                ]
            ],
            'plugins' =>[
                'exception_handler' =>[
                    'throw_exceptions' => true
                ],
            ],
        ],*/
        'zendservershm' =>[
            'adapter' =>[
                'name'     =>'zendservershm',
                'options'  =>[
                    'namespace' => 'WSRADARLOCAL',
                    'ttl' => 43200,
                ]
            ],
            'plugins' =>[
                'exception_handler' =>[
                    'throw_exceptions' => true
                ],
            ]
        ]
    ],

    'doctrine' =>[
        'connection' =>[
            'orm_default' =>[
                'params' =>[
                    'host' => '10.37.0.157',
                    'user' => 'sysdbeproinfo_hmg',
                    'password' => 'sysdbeproinfo_hmg',
                    'dbname' => 'dbeproinfo_hmg',
                    'port' => '5432',
                    'charset' => 'utf8'
                ]
            ]
        ],
        'configuration' =>[
            'orm_default' =>[
                'proxy_dir' => realpath(__DIR__ . '/../../') . '/data/proxy/DoctrineORMModule/'
            ]
        ]
    ],
    'view_manager' =>[
        'display_not_found_reason' => true,
        'display_exceptions' => true
    ],

    'oauth2' =>[
        'options' =>[
            'access_lifetime' => 7200
        ]
    ],
    'storage' => [
        'path' => '/opt/radar/storage/'
    ]
];