<?php
return [
    'doctrine.configuration.orm_default' => function ($srvMaganer) {
        /** @comment comando para gerar entidade:
         $./vendor/doctrine/doctrine-module/bin/doctrine-module
          orm:convert-mapping --namespace="Application\\Entity\\"
          --force  --from-database annotation module/Application/src/
         */
        $factory = new \DoctrineORMModule\Service\ConfigurationFactory('orm_default');
        /**
         * @var \Doctrine\ORM\Configuration $config
         */
        $config = $factory->createService($srvMaganer);
        // $config->setFilterSchemaAssetsExpression('/^tb_eproinfo_turma$/'); //Descomentar para gerar entity
        return $config;
    },
  
  'UtilService' => function () {
    $srvUtil = new \RADAR\Service\UtilService();
    return $srvUtil;
  },
  
  'UsuarioService' => function ($serviceMamager) {
    $srvUsuario = new \RADAR\Service\UsuarioService();
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $serviceMamager->get('doctrine.entitymanager.orm_default');
    $userRepository = $entityManager->getRepository('RADAR\Entity\TbRadarUsuario');
    $srvUsuario->setConfig(
      $serviceMamager->get('config')
    );
    $srvUsuario->setSrvUtil(
      $serviceMamager->get('UtilService')
    );
    $srvUsuario->setRepository($userRepository);
    return $srvUsuario;
  },
  
  'AreaService' => function ($serviceManager) {
    $srv = new \RADAR\Service\AreaService();
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
    $repository = $entityManager->getRepository('RADAR\Entity\TbRadarArea');
    $srv->setConfig(
      $serviceManager->get('config')
    );
    $srv->setSrvUtil(
      $serviceManager->get('UtilService')
    );
    $srv->setRepository($repository);
    return $srv;
  },
  
  'TimeService' => function ($serviceManager) {
    $srv = new \RADAR\Service\TimeService();
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
    $repository = $entityManager->getRepository('RADAR\Entity\TbRadarTime');
    $srv->setConfig(
      $serviceManager->get('config')
    );
    $srv->setSrvUtil(
      $serviceManager->get('UtilService')
    );
    $srv->setRepository($repository);
    return $srv;
  },
  
  'PerfilService' => function ($serviceManager) {
    $srv = new \RADAR\Service\PerfilService();
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
    $repository = $entityManager->getRepository('RADAR\Entity\TbRadarPerfil');
    $srv->setConfig(
      $serviceManager->get('config')
    );
    $srv->setSrvUtil(
      $serviceManager->get('UtilService')
    );
    $srv->setRepository($repository);
    return $srv;
  },
  
  'AtividadeService' => function ($serviceManager) {
    $srv = new \RADAR\Service\AtividadeService();
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
    $repository = $entityManager->getRepository('RADAR\Entity\TbRadarAtividade');
    $srv->setConfig(
      $serviceManager->get('config')
    );
    $srv->setSrvUtil(
      $serviceManager->get('UtilService')
    );
    $srv->setRepository($repository);
    return $srv;
  }
];