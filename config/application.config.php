<?php

return [
    
    'modules' => include __DIR__ . '/modules.config.php',
    'module_listener_options' => [
        'module_paths' => [
            './module',
            './vendor'
        ],
        // Using __DIR__ to ensure cross-platform compatibility. Some platforms --
        // e.g., IBM i -- have problems with globs that are not qualified.
        'config_glob_paths' => [
            realpath(__DIR__) . '/autoload/{,*.}{global,local}.php',
            getenv('APP_CONFIG_FILE') ?: 'config/autoload/desenvolvedor.php'
        ],
        'config_cache_key' => 'application.config.cache',
        'config_cache_enabled' => false,
        'module_map_cache_key' => 'application.module.cache',
        'module_map_cache_enabled' => false,
    ]
];
