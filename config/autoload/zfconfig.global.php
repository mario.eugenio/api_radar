<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c] 2014 Zend Technologies USA Inc. (http://www.zend.com]
 */
return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'eventmanager' => 'orm_default',
              'driverClass' => \Doctrine\DBAL\Driver\PDOMySql\Driver::class,
              'doctrine_type_mappings' => []
            ]
        ],
        'entitymanager' => [
            'orm_default' => [
                'connection' => 'orm_default',
                'configuration' => 'orm_default'
            ]
        ],
        'configuration' => [
            'orm_default' => [
                'types' => [
                ]
            ]
        ]
    ],

    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ],

    'service_manager' => [
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
        ]
    ],

    'cacheApplicationNamespace' => '_EPROINFO_WS_'
];
