<?php
namespace RADAR\Service;

use Application\Exception\ValidationException;
use Application\Service\AbstractService;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManager;
use RADAR\Constantes\AtividadeFase;
use RADAR\Entity\TbRadarAtividade;
use RADAR\Entity\TbRadarAtividadeEnvolvido;
use RADAR\Entity\TbRadarHistAtividadeFase;
use RADAR\Repository\UsuarioRepository;

/**
 * Created by PhpStorm.
 * User: marioeugenio
 * Date: 10/21/17
 * Time: 10:11 PM
 */
class AtividadeService extends AbstractService
{
  private $config;
  /** @var  UtilService */
  private $srvUtil;
  
  public function estatisticasAtividadesQuantitativos($idArea)
  {
    $params = ['_AREA' => $idArea];
    
    $em = $this->getDefaultEntityManager();
    $conn = $em->getConnection();
    $sql = "SELECT (SELECT COUNT(*)
      FROM tb_radar_atividade at
      JOIN tb_radar_hist_atividade_fase haf on at.co_atividade = haf.co_atividade
      WHERE haf.co_fase = 1
      AND haf.dt_fechamento IS NULL
      AND at.co_area = :_AREA) as 'pendentes',
      (SELECT COUNT(*)
      FROM tb_radar_atividade at
      JOIN tb_radar_hist_atividade_fase haf on at.co_atividade = haf.co_atividade
      WHERE haf.co_fase = 2
      AND haf.dt_fechamento IS NULL
      AND at.co_area = :_AREA) as 'desenvolvimento',
      (SELECT COUNT(*)
      FROM tb_radar_atividade at
      JOIN tb_radar_hist_atividade_fase haf on at.co_atividade = haf.co_atividade
      WHERE at.co_area = :_AREA
      AND haf.co_fase IN (1,2)
      ) as 'total' ";
    
    $statement = $conn->prepare($sql);
    $statement->execute($params);
    
    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }
  
  public function estatisticasAtividades($idUsuario)
  {
    $params = ['_USUARIO' => $idUsuario];
  
    $em = $this->getDefaultEntityManager();
    $conn = $em->getConnection();
    $sql = "SELECT (COUNT(*) * 100 / (SELECT COUNT(*) FROM tb_radar_atividade WHERE co_usuario_executor = :_USUARIO)) as atrasadas, COUNT(*) as numeroAtrasadas,
      ((SELECT COUNT(*) FROM tb_radar_atividade WHERE st_ativo = 1 AND dt_entrega >= date('now') AND co_usuario_executor = :_USUARIO) * 100 /
      (SELECT COUNT(*) FROM tb_radar_atividade WHERE co_usuario_executor = :_USUARIO AND st_ativo = 1)) as pendentes,
      (SELECT COUNT(*) FROM tb_radar_atividade WHERE st_ativo = 1 AND dt_entrega >= date('now') AND co_usuario_executor = :_USUARIO) as numeroPendentes
      FROM tb_radar_atividade at
      JOIN tb_radar_hist_atividade_fase hf on at.co_atividade = hf.co_atividade
      WHERE at.st_ativo = 1
      AND at.dt_entrega <= date('now')
      AND at.co_usuario_executor = :_USUARIO
      AND (hf.dt_fechamento IS NULL AND hf.co_fase = 2) ";

    $statement = $conn->prepare($sql);
    $statement->execute($params);
  
    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }
  
  public function historicoAtividades($month, $year)
  {
    $em = $this->getDefaultEntityManager();
    $emConfig = $em->getConfiguration();
    $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
    $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
    
    $qb   = $em->createQueryBuilder();
  
    $qb->select('A, AR, UE, UD, EX, CM, EN, AN, HS')
      ->from('RADAR\Entity\TbRadarAtividade', 'A')
      ->leftJoin('A.coArea', 'AR')
      ->leftJoin('A.coUsuarioEnvolvido', 'UE')
      ->leftJoin('A.coUsuarioDemandante', 'UD')
      ->leftJoin('A.coUsuarioExecutor', 'EX')
      ->leftJoin('A.comentarios', 'CM')
      ->leftJoin('A.envolvidos', 'EN')
      ->leftJoin('A.anexo', 'AN')
      ->leftJoin('A.historicoFase', 'HS')
      ->join('A.historicoFase', 'HF')
      ->where('MONTH(A.dtEntrega) = :month')
      ->andWhere('YEAR(A.dtEntrega) = :year')
      ->andWhere('A.stAtivo = :ativo')
      ->andWhere($qb->expr()->in('HF.coFase', [AtividadeFase::ARQUIVADA, AtividadeFase::CONCLUIDA]))
      ->andWhere('HF.dtFechamento is NULL')
      ->setParameter('month', $month)
      ->setParameter('year', $year)
      ->setParameter('ativo', true);
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function listarAtividadesSeguindo($idUsuario)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('A, AR, UE, UD, EX, CM, EN, AN, HS')
      ->from('RADAR\Entity\TbRadarAtividade', 'A')
      ->leftJoin('A.coArea', 'AR')
      ->leftJoin('A.coUsuarioEnvolvido', 'UE')
      ->leftJoin('A.coUsuarioDemandante', 'UD')
      ->leftJoin('A.coUsuarioExecutor', 'EX')
      ->leftJoin('A.comentarios', 'CM')
      ->leftJoin('A.envolvidos', 'EN')
      ->leftJoin('A.anexo', 'AN')
      ->leftJoin('A.historicoFase', 'HS')
      ->join('A.historicoFase', 'HF')
      ->where('A.coUsuarioEnvolvido = :usuario')
      ->andWhere('A.stAtivo = :ativo')
      ->andWhere('HF.coFase = :fase')
      ->andWhere('HF.dtFechamento is NULL')
      ->setParameter('usuario', $idUsuario)
      ->setParameter('fase', AtividadeFase::EM_ATENDIMENTO)
      ->setParameter('ativo', true);
    
    return $qb->getQuery()->getArrayResult();
  }
  
  public function listarAtividadesDisponiveis($idArea)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('A, AR, UE, UD, EX, CM, EN, AN, HS')
      ->from('RADAR\Entity\TbRadarAtividade', 'A')
      ->leftJoin('A.coArea', 'AR')
      ->leftJoin('A.coUsuarioEnvolvido', 'UE')
      ->leftJoin('A.coUsuarioDemandante', 'UD')
      ->leftJoin('A.coUsuarioExecutor', 'EX')
      ->leftJoin('A.comentarios', 'CM')
      ->leftJoin('A.envolvidos', 'EN')
      ->leftJoin('A.anexo', 'AN')
      ->leftJoin('A.historicoFase', 'HS')
      ->join('A.historicoFase', 'HF')
      ->where('A.coArea = :area')
      ->andWhere('A.stAtivo = :ativo')
      ->andWhere('HF.coFase = :fase')
      ->andWhere('HF.dtFechamento is NULL')
      ->setParameter('area', $idArea)
      ->setParameter('fase', AtividadeFase::ABERTA)
      ->setParameter('ativo', true);
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function listarAtividadesPendentes($idArea, $idUsuario)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('A, AR, UE, UD, EX, CM, EN, AN, HS')
      ->from('RADAR\Entity\TbRadarAtividade', 'A')
      ->leftJoin('A.coArea', 'AR')
      ->leftJoin('A.coUsuarioEnvolvido', 'UE')
      ->leftJoin('A.coUsuarioDemandante', 'UD')
      ->leftJoin('A.coUsuarioExecutor', 'EX')
      ->leftJoin('A.comentarios', 'CM')
      ->leftJoin('A.envolvidos', 'EN')
      ->leftJoin('A.anexo', 'AN')
      ->leftJoin('A.historicoFase', 'HS')
      ->join('A.historicoFase', 'HF')
      ->where('A.coArea = :area')
      ->andWhere('A.dtEntrega < :now')
      ->andWhere('A.coUsuarioExecutor = :executor')
      ->andWhere('A.stAtivo = :ativo')
      ->andWhere('HF.coFase = :fase')
      ->andWhere('HF.dtFechamento is NULL')
      ->setParameter('area', $idArea)
      ->setParameter('now', new \DateTime())
      ->setParameter('executor', $idUsuario)
      ->setParameter('fase', AtividadeFase::EM_ATENDIMENTO)
      ->setParameter('ativo', true);
  
    return $qb->getQuery()->getArrayResult();
  }
  public function listarAtividadesPendentesPorArea($idArea)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('A, AR, UE, UD, EX, CM, EN, AN, HS')
      ->from('RADAR\Entity\TbRadarAtividade', 'A')
      ->leftJoin('A.coArea', 'AR')
      ->leftJoin('A.coUsuarioEnvolvido', 'UE')
      ->leftJoin('A.coUsuarioDemandante', 'UD')
      ->leftJoin('A.coUsuarioExecutor', 'EX')
      ->leftJoin('A.comentarios', 'CM')
      ->leftJoin('A.envolvidos', 'EN')
      ->leftJoin('A.anexo', 'AN')
      ->leftJoin('A.historicoFase', 'HS')
      ->join('A.historicoFase', 'HF')
      ->where('A.coArea = :area')
      ->andWhere('A.dtEntrega < :now')
      ->andWhere('A.coUsuarioExecutor is NULL')
      ->andWhere('A.stAtivo = :ativo')
      ->andWhere('HF.coFase = :fase')
      ->andWhere('HF.dtFechamento is NULL')
      ->setParameter('area', $idArea)
      ->setParameter('now', new \DateTime())
      ->setParameter('fase', AtividadeFase::EM_ATENDIMENTO)
      ->setParameter('ativo', true);
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function listarMinhasAtividades($idArea, $idUsuario)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('A, AR, UE, UD, EX, CM, EN, AN, HS')
      ->from('RADAR\Entity\TbRadarAtividade', 'A')
      ->leftJoin('A.coArea', 'AR')
      ->leftJoin('A.coUsuarioEnvolvido', 'UE')
      ->leftJoin('A.coUsuarioDemandante', 'UD')
      ->leftJoin('A.coUsuarioExecutor', 'EX')
      ->leftJoin('A.comentarios', 'CM')
      ->leftJoin('A.envolvidos', 'EN')
      ->leftJoin('A.anexo', 'AN')
      ->leftJoin('A.historicoFase', 'HS')
      ->join('A.historicoFase', 'HF')
      ->where('A.coArea = :area')
      ->andWhere('A.coUsuarioExecutor = :executor')
      ->andWhere('A.stAtivo = :ativo')
      ->andWhere('HF.coFase = :fase')
      ->andWhere('HF.dtFechamento is NULL')
      ->setParameter('area', $idArea)
      ->setParameter('executor', $idUsuario)
      ->setParameter('fase', AtividadeFase::EM_ATENDIMENTO)
      ->setParameter('ativo', true);
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function listarAreaAtividades($idArea)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('A, AR, UE, UD, EX, CM, EN, AN, HS')
      ->from('RADAR\Entity\TbRadarAtividade', 'A')
      ->leftJoin('A.coArea', 'AR')
      ->leftJoin('A.coUsuarioEnvolvido', 'UE')
      ->leftJoin('A.coUsuarioDemandante', 'UD')
      ->leftJoin('A.coUsuarioExecutor', 'EX')
      ->leftJoin('A.comentarios', 'CM')
      ->leftJoin('A.envolvidos', 'EN')
      ->leftJoin('A.anexo', 'AN')
      ->leftJoin('A.historicoFase', 'HS')
      ->join('A.historicoFase', 'HF')
      ->where('A.coArea = :area')
      ->andWhere('A.coUsuarioExecutor is NULL')
      ->andWhere('A.stAtivo = :ativo')
      ->andWhere('HF.coFase = :fase')
      ->andWhere('HF.dtFechamento is NULL')
      ->setParameter('area', $idArea)
      ->setParameter('fase', AtividadeFase::EM_ATENDIMENTO)
      ->setParameter('ativo', true);
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function pesquisarAtividades($word)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
  
    $qb->select('A, AR, UE, UD, EX, CM, EN, AN, HS')
      ->from('RADAR\Entity\TbRadarAtividade', 'A')
      ->leftJoin('A.coArea', 'AR')
      ->leftJoin('A.coUsuarioEnvolvido', 'UE')
      ->leftJoin('A.coUsuarioDemandante', 'UD')
      ->leftJoin('A.coUsuarioExecutor', 'EX')
      ->leftJoin('A.comentarios', 'CM')
      ->leftJoin('A.envolvidos', 'EN')
      ->leftJoin('A.anexo', 'AN')
      ->leftJoin('A.historicoFase', 'HS')
      ->join('A.historicoFase', 'HF')
      ->where('LOWER(A.nomeAtividade) LIKE :word')
      ->orWhere('LOWER(A.dsAtividade) LIKE :word')
      ->andWhere('A.stAtivo = :ativo')
      ->andWhere('HF.coFase = :fase')
      ->andWhere('HF.dtFechamento is NULL')
      ->setParameter('word', '%' + strtolower($word) + '%')
      ->setParameter('fase', AtividadeFase::EM_ATENDIMENTO)
      ->setParameter('ativo', true);
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function _validate(TbRadarAtividade $entity)
  {
    if (!$entity->getNomeAtividade()) {
      throw new ValidationException('Nome é obrigatório');
    }
    
    if (!$entity->getDsAtividade()) {
      throw new ValidationException('Descrição é obrigatório');
    }
  
    if (!$entity->getDtEntrega()) {
      throw new ValidationException('Data de entrega é obrigatório');
    }
  
    if (!$entity->getCoArea()) {
      throw new ValidationException('Área é obrigatório');
    }
  }
  
  public function save($data, $id = 0)
  {
    $em = $this->getDefaultEntityManager();
    //$em->beginTransaction();
  
    try {
      $data = json_decode(json_encode($data), true);
      $entity = new TbRadarAtividade($data);

      if($id > 0) {
        $entity = $this->repository->find($id);
        $entity->setData($data);
      }
  
      $this->_validate($entity);
      
      $entity->setStAtivo(true);
      $entity->setCoUsuarioDemandante(
        $this->_getRepositoryUsuario()->find(
          (int) $entity->getCoUsuarioDemandante()
        )
      );
  
      $entity->setCoArea(
        $this->_getRepositoryArea()->find(
          (int) $entity->getCoArea()
        )
      );
  
      if (isset($data['listExecutor'])) {
        $listExecutor = json_decode($data['listExecutor'], true);
        if (count($listExecutor)) {
          $entity->setCoUsuarioExecutor(
            $this->_getRepositoryUsuario()->find($listExecutor[0])
          );
        }
      }
  
      $em->persist($entity);
      $em->flush();
      
      if (isset($data['listEnvolvidos'])) {
        $listEnvolvidos = json_decode($data['listEnvolvidos'], true);
        $this->_adicionarEnvolvidos($entity, $listEnvolvidos);
      }
  
      $this->atualizarHistoricoFase($entity->getCoAtividade(), AtividadeFase::ABERTA);
  
      $em->commit();
      
    } catch (Exception $e) {
    //  $em->rollBack();
      throw $e;
    }
    
    return $entity;
  }
  
  private function _adicionarEnvolvidos(TbRadarAtividade $atividade, $list)
  {
    if (count($list)) {
      $em = $this->getDefaultEntityManager();
      $em->getConnection()->beginTransaction();
      
     foreach ($list as $item) {
       $entity = new TbRadarAtividadeEnvolvido();
       $entity->setCoEnvolvido($item);
       $entity->setCoAtividade($atividade->getCoAtividade());
  
       $em->persist($entity);
       $em->flush();
     }
    }
  }
  
  public function concluirAtividade($idAtividade)
  {
    $this->atualizarHistoricoFase($idAtividade, AtividadeFase::CONCLUIDA);
  }
  
  public function excluirAtividade($idAtividade)
  {
    $this->atualizarHistoricoFase($idAtividade, AtividadeFase::CANCELADA);
  }
  
  public function sairAtividade($idAtividade)
  {
    /** @var TbRadarAtividade $atividade */
    $atividade = $this->repository->find($idAtividade);
  
    if($atividade instanceof TbRadarAtividade) {
      $em = $this->getDefaultEntityManager();
    
      $atividade->setCoUsuarioExecutor(null);
    
      $em->persist($atividade);
      $em->flush();
    
      $this->atualizarHistoricoFase($idAtividade, AtividadeFase::EM_ATENDIMENTO);
    
      return $atividade;
    }
  
    throw new \Exception('Atividade não encontrada');
  }
  
  public function executorAtividade($idAtividade, $idUsuario)
  {
    /** @var TbRadarAtividade $atividade */
    $atividade = $this->repository->find($idAtividade);
  
    if($atividade instanceof TbRadarAtividade) {
      $em = $this->getDefaultEntityManager();
  
      $atividade->setCoUsuarioExecutor(
        $this->_getRepositoryUsuario()->find($idUsuario)
      );
  
      $em->persist($atividade);
      $em->flush();
      
      $this->atualizarHistoricoFase($idAtividade, AtividadeFase::EM_ATENDIMENTO);
  
      return $atividade;
    }
  
    throw new \Exception('Atividade não encontrada');
  }
  
  public function seguirAtividade($idAtividade, $idUsuario)
  {
    /** @var TbRadarAtividade $atividade */
    $atividade = $this->repository->find($idAtividade);
    
    if($atividade instanceof TbRadarAtividade) {
      $em = $this->getDefaultEntityManager();
      
      $atividade->setCoUsuarioEnvolvido(
        $this->_getRepositoryUsuario()->find($idUsuario)
      );
  
      $em->persist($atividade);
      $em->flush();
      
      return $atividade;
    }
    
    throw new \Exception('Atividade não encontrada');
  }
  
  public function arquivarAtividade($idAtividade)
  {
    $this->atualizarHistoricoFase($idAtividade, AtividadeFase::ARQUIVADA);
  }
  
  public function atualizarHistoricoFase ($idAtividade, $idFase)
  {
    $em = $this->getDefaultEntityManager();
    
      $qb = $em->createQueryBuilder();
      $qb->select('H')
        ->from('RADAR\Entity\TbRadarHistAtividadeFase', 'H')
        ->where('H.dtFechamento = :null')
        ->andWhere('H.coAtividade = :atividade')
        ->setParameter('null', null)
        ->setParameter('atividade', $idAtividade);
      
      $entity = $qb->getQuery()->getResult();
      
      if(count($entity)) {
        $entity = $entity[0];
        
        if ($entity instanceof TbRadarHistAtividadeFase) {
          $em = $this->getDefaultEntityManager();
    
          $entity->setDtFechamento(new \DateTime());
    
          $em->persist($entity);
          $em->flush();
        }
      }
      
      
      $this->_criarAtualizarFase($idAtividade, $idFase);
  }
  
  private function _criarAtualizarFase($idAtividade, $idFase)
  {
    $em = $this->getDefaultEntityManager();
    
    $entity = new TbRadarHistAtividadeFase();
    $entity->setStAtivo(true);
    $entity->setDtEntrada(new \DateTime());
    $entity->setCoAtividade(
      $this->repository->find($idAtividade)
    );
    $entity->setCoFase($idFase);
  
    $em->persist($entity);
    $em->flush();
  
  }
  
  /**
   * @param mixed $config
   */
  public function setConfig($config)
  {
    $this->config = $config;
  }
  
  public function setSrvUtil($srvUtil)
  {
    $this->srvUtil = $srvUtil;
  }
  
  private function _getRepositoryHistFase() {
    return $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarHistAtividadeFase');
  }
  
  private function _getRepositoryUsuario() {
    return $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarUsuario');
  }
  
  private function _getRepositoryArea() {
    return $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarArea');
  }
}