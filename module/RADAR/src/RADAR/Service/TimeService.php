<?php
namespace RADAR\Service;

use Application\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use RADAR\Repository\TimeRepository;
use RADAR\Repository\UsuarioRepository;

/**
 * Created by PhpStorm.
 * User: marioeugenio
 * Date: 10/21/17
 * Time: 10:11 PM
 */
class TimeService extends AbstractService
{
  private $config;
  /** @var  UtilService */
  private $srvUtil;
  
  public function listTimes()
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('P')
      ->from('RADAR\Entity\TbRadarTime', 'P');
  
    return $qb->getQuery()->getArrayResult();
  }
  
  /**
   * @param mixed $config
   */
  public function setConfig($config)
  {
    $this->config = $config;
  }
  
  public function setSrvUtil($srvUtil)
  {
    $this->srvUtil = $srvUtil;
  }
}