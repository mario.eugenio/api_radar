<?php
namespace RADAR\Service;

use Application\Exception\ValidationException;
use Application\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use RADAR\Entity\TbRadarMembrosTime;
use RADAR\Entity\TbRadarUsuario;
use RADAR\Repository\UsuarioRepository;
use Zend\Validator\EmailAddress;

/**
 * Created by PhpStorm.
 * User: marioeugenio
 * Date: 10/21/17
 * Time: 10:12 PM
 */
class UsuarioService extends AbstractService
{
  private $config;
  /** @var  UtilService */
  private $srvUtil;
  
  private function _validEmail(TbRadarUsuario $entity)
  {
    $validator = new EmailAddress();
    if (!$validator->isValid($entity->getEmailUsuario())) {
      throw new ValidationException('E-mail inválido');
    }
  }
  
  private function _validateAuth(TbRadarUsuario $entity)
  {
    if (!$entity->getEmailUsuario()) {
      throw new ValidationException('E-mail é obrigatório');
    }
  
    $this->_validEmail($entity);
    
    if (!$entity->getSenha()) {
      throw new ValidationException('Senha é obrigatório');
    }
  }
  
  private function _checkUserTime($user)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('MT, P')
      ->from('RADAR\Entity\TbRadarMembrosTime', 'MT')
      ->join('MT.coPerfil', 'P')
      ->where('MT.coUsuario = :id')
      ->setParameter('id', $user['coUsuario']);
    
    $membrosTime = $qb->getQuery()->getResult();
    $result = [];
    
    /** @var TbRadarMembrosTime $times */
    foreach ($membrosTime as $times) {
      $result[] = array(
        'coTime' => $times->getCoTime()->getCoTime(),
        'noTime' => $times->getCoTime()->getNoTime(),
        'coPerfil' => $times->getCoPerfil()->getCoPerfil(),
        'noPerfil' => $times->getCoPerfil()->getNoPerfil()
      );
    }
  
    return $result;
  }
  
  public function auth($params = array())
  {
    $data = json_decode(json_encode($params), true);
    $entity = new TbRadarUsuario($data);
    
    $this->_validateAuth($entity);
  
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('U.coUsuario, U.noUsuario, U.emailUsuario, U.telUsuario')
      ->from('RADAR\Entity\TbRadarUsuario', 'U')
      ->where('LOWER(U.emailUsuario) LIKE :email')
      ->andWhere('U.senha = :senha')
      ->andWhere('U.stAtivo = 1')
      ->setParameter('email', strtolower($entity->getEmailUsuario()))
      ->setParameter('senha', $entity->getSenha());
  
    $result = $qb->getQuery()->getArrayResult();

    if (!count($result)) {
      throw new \Exception('Usuário ou senha inválido');
    }
    
    $user = $result[0];
    $user['membroTimes'] = $this->_checkUserTime($user);
    
    return $user;
  }
  
  public function _validate(TbRadarUsuario $entity)
  {
    if (!$entity->getNoUsuario()) {
      throw new ValidationException('Nome é obrigatório');
    }
  
    if (!$entity->getEmailUsuario()) {
      throw new ValidationException('E-mail é obrigatório');
    }
  
    $this->_validEmail($entity);
  
    if (!$entity->getTelUsuario()) {
      throw new ValidationException('Telefone é obrigatório');
    }
  
    if (!$entity->getCoUsuario()) {
      if (!$entity->getSenha()) {
        throw new ValidationException('Senha é obrigatório');
      }
    }
    
  }
  
  public function save($data = array(), $id = 0)
  {
    $em = $this->getDefaultEntityManager();
    //$em->beginTransaction();
    
    try {
  
      $data = json_decode(json_encode($data), true);
      $entity = new TbRadarUsuario($data);
  
      if ($id > 0) {
        $entity = $this->repository->find($id);
        $entity->setData($data);
      }
      
      $perfil = json_decode($data['perfilSelecionado'], true);
      $areas = json_decode($data['areasSelecionadas'], true);
      
      $this->_validate($entity);
      if (!isset($data['perfilSelecionado'])) {
        throw new ValidationException('Selecione um perfil');
      }
  
      if (!isset($data['areasSelecionadas'])) {
        throw new ValidationException('Selecione uma aréa');
      }
  
      $em->persist($entity);
      $em->flush();
      
      $this->_addMembroTimes($entity, $perfil, $areas);
  
  
    } catch(\Exception $ex) {
     // $em->rollback();
      throw $ex;
    }
    
    return $entity;
  }
  
  private function _addMembroTimes($usuario, $perfil, $listaTimes)
  {
    try {
      if (count($listaTimes)) {
        $em = $this->getDefaultEntityManager();
        $em->beginTransaction();
        
        foreach ($listaTimes as $item) {
          $entity = new TbRadarMembrosTime();
          $entity->setCoTime($this->_getRepositoryTime()->find(1));
          $entity->setNoTime($perfil['noArea']);
          $entity->setDsTime($perfil['dsArea']);
          $entity->setCoUsuario($usuario);
          $entity->setCoArea($this->_getRepositoryArea()->find($perfil['coArea']));
          $entity->setCoPerfil($this->_getRepositoryPerfil()->find($item));
      
          $em->persist($entity);
          $em->flush();
        }
    
        $em->commit();
      }
      
    } catch (\Exception $ex) {
      $em->rollback();
      throw $ex;
    }
  }
  
  public function search($text)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
    
    $word = '%' . strtolower($text) . '%';
  
    $qb->select('U')
      ->from('RADAR\Entity\TbRadarUsuario', 'U')
      ->where('LOWER(U.noUsuario) LIKE :word')
      ->orWhere('LOWER(U.emailUsuario) LIKE :word')
      ->setParameter('word', $word);
    
    return $qb->getQuery()->getArrayResult();
  }
  
  public function getUsuario($id)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
    
    $qb->select('U')
      ->from('RADAR\Entity\TbRadarUsuario', 'U')
      ->where('U.coUsuario = :id')
      ->setParameter('id', $id);
  
    $result = $qb->getQuery()->getSingleResult();
    
    if(!$result) {
      throw new \Exception('Usuário não encontrado');
    }
    
    return $result;
  }
  
  public function listaUsuario()
  {
    /** @var UsuarioRepository $useRepo */
    $useRepo = $this->repository;
    $arrResult = $useRepo->findAll();
    return $arrResult;
  }
  
  public function getUsuarioMembrosTime($idUsuario)
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('MT, P, A')
      ->from('RADAR\Entity\TbRadarMembrosTime', 'MT')
      ->join('MT.coPerfil', 'P')
      ->join('MT.coArea', 'A')
      ->where('MT.coUsuario = :id')
      ->andWhere('MT.stAtivo = 1')
      ->setParameter('id', $idUsuario);
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function getUsuarioAtivos()
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('U.coUsuario', 'U.noUsuario', 'P.coPerfil', 'P.noPerfil', 'A.coArea', 'A.noArea', 'A.dsArea', 'A.dtCriacao')
      ->from('RADAR\Entity\TbRadarMembrosTime', 'MT')
      ->join('MT.coPerfil', 'P')
      ->join('MT.coArea', 'A')
      ->join('MT.coUsuario', 'U')
      ->where('MT.stAtivo = 1')
      ->andWhere('A.stAtivo = 1');
    
    $result = $qb->getQuery()->getArrayResult();
    $arr = array();
    
    foreach ($result as $key => $value) {
        $arr[$value['noArea']]['users'][] = $value;
    }

    return $arr;
  }
  
  public function getUsuarioInativos()
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('U.coUsuario', 'U.noUsuario', 'P.coPerfil', 'P.noPerfil', 'A.coArea', 'A.noArea', 'A.dsArea', 'A.dtCriacao')
      ->from('RADAR\Entity\TbRadarMembrosTime', 'MT')
      ->join('MT.coPerfil', 'P')
      ->join('MT.coArea', 'A')
      ->join('MT.coUsuario', 'U')
      ->where('MT.stAtivo is NULL')
      ->andWhere('A.stAtivo = 1');
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function setAprovarUsuario($idUsuario, $idArea, $idPerfil, $aprovacao = true)
  {
    $em = $this->getDefaultEntityManager();
    $em->beginTransaction();
    
    try {
      $repository = $this->_getRepositoryMembroTimes();
  
      /** @var TbRadarMembrosTime $entity */
      $entity = $repository->findOneBy(array('coUsuario' => $idUsuario, 'coArea' => $idArea, 'coPerfil' => $idPerfil));
  
      if ($entity) {
        $entity->setStAtivo($aprovacao);
        
        if ($aprovacao == 1) {
          $repUsuario = $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarUsuario');
          /** @var TbRadarUsuario $user */
          $user = $repUsuario->find($idUsuario);
          
          if ($user) {
            $user->setStAtivo(1);
            $em->persist($user);
            $em->flush();
          }
        }
  
        $em->persist($entity);
        $em->flush();
  
        $em->commit();
      }
    } catch (\Exception $ex) {
      $em->rollback();
      throw $ex;
    }
  }
  
  public function inativarUsuario($id)
  {
    $em = $this->getDefaultEntityManager();
    $em->beginTransaction();
  
    try {
      $repository = $this->_getRepositoryMembroTimes();
      /** @var TbRadarMembrosTime $entity */
      $entity = $repository->find($id);
    
      if ($entity) {
        $entity->setStAtivo(0);
      
        $em->persist($entity);
        $em->flush();
      
        $em->commit();
      }
    } catch (\Exception $ex) {
      $em->rollback();
      throw $ex;
    }
  }
  
  /**
   * @return \Doctrine\ORM\EntityRepository
   */
  private function _getRepositoryMembroTimes() {
    return $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarMembrosTime');
  }
  
  private function _getRepositoryPerfil() {
    return $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarPerfil');
  }
  
  private function _getRepositoryTime() {
    return $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarTime');
  }
  
  private function _getRepositoryArea() {
    return $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarArea');
  }
  
  /**
   * @param mixed $config
   */
  public function setConfig($config)
  {
    $this->config = $config;
  }
  
  public function setSrvUtil($srvUtil)
  {
    $this->srvUtil = $srvUtil;
  }
}