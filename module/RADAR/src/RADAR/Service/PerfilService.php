<?php
namespace RADAR\Service;

use Application\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use RADAR\Repository\FuncionalidadeRepository;
use RADAR\Repository\PerfilRepository;
use RADAR\Repository\UsuarioRepository;

/**
 * Created by PhpStorm.
 * User: marioeugenio
 * Date: 10/21/17
 * Time: 10:11 PM
 */
class PerfilService extends AbstractService
{
  private $config;
  /** @var  UtilService */
  private $srvUtil;
  
  public function listPerfil()
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('P')
      ->from('RADAR\Entity\TbRadarPerfil', 'P')
      ->where('P.coPerfil <> 4');
    
    return $qb->getQuery()->getArrayResult();
    
    return $arrResult;
  }
  
  public function listFuncionalidades()
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('P')
      ->from('RADAR\Entity\TbRadarFuncionalidade', 'P');
  
    return $qb->getQuery()->getArrayResult();
  }
  
  /**
   * @param mixed $config
   */
  public function setConfig($config)
  {
    $this->config = $config;
  }
  
  public function setSrvUtil($srvUtil)
  {
    $this->srvUtil = $srvUtil;
  }
  
  private function _getRepositoryFuncionalidades() {
    return $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarFuncionalidade');
  }
}