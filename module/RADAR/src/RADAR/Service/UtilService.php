<?php

namespace RADAR\Service;


class UtilService
{
    public function base64ToJpeg($base64String, $outputFile) {
        $base64String = str_replace('data:image/jpeg;base64,', '', $base64String);
        $base64String = str_replace(' ', '+', $base64String);
        $data = base64_decode($base64String);
        $file = $outputFile;
        $success = file_put_contents($file, $data);
        if (!$success) {
            throw new \Exception('Ocorreu um erro ao tentar salvar a imagem', 400);
        }
        return $success;
    }

    /**
     * @param $strSenha
     * @return string
     */
    public function encryptPass($strSenha)
    {
        return base64_encode(hash('sha256', $strSenha, true));
    }
}