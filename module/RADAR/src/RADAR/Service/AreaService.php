<?php
namespace RADAR\Service;

use Application\Service\AbstractService;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManager;
use RADAR\Entity\TbRadarArea;
use RADAR\Repository\UsuarioRepository;

/**
 * Created by PhpStorm.
 * User: marioeugenio
 * Date: 10/21/17
 * Time: 10:11 PM
 */
class AreaService extends AbstractService
{
  private $config;
  /** @var  UtilService */
  private $srvUtil;
  
  public function listAreas()
  {
    $em = $this->getDefaultEntityManager();
    $qb   = $em->createQueryBuilder();
  
    $qb->select('P')
      ->from('RADAR\Entity\TbRadarArea', 'P')
      ->where('P.stAtivo = 1');
  
    return $qb->getQuery()->getArrayResult();
  }
  
  public function _validate(TbRadarArea $entity)
  {
    if (!$entity->getNoUsuario()) {
      throw new ValidationException('Nome é obrigatório');
    }
    
    if (!$entity->getEmailUsuario()) {
      throw new ValidationException('E-mail é obrigatório');
    }
    
    $this->_validEmail($entity);
    
    if (!$entity->getTelUsuario()) {
      throw new ValidationException('Telefone é obrigatório');
    }
    
    if (!$entity->getCoUsuario()) {
      if (!$entity->getSenha()) {
        throw new ValidationException('Senha é obrigatório');
      }
    }
    
  }
  
  public function save($data = array(), $id = 0)
  {
    $em = $this->getDefaultEntityManager();
    $em->beginTransaction();
    
    try {
      $data = json_decode(json_encode($data), true);
      
      $entity = new TbRadarArea($data);
  
      if ($id > 0) {
        $entity = $this->repository->find($id);
        $entity->setData($data);
      }
  
      $entity->setStAtivo(1);
  
      $em->persist($entity);
      $em->flush();
      
      $em->commit();
      
    } catch(\Exception $ex) {
      $em->rollback();
      throw $ex;
    }
  }
  
  public function inativarArea($id)
  {
    $em = $this->getDefaultEntityManager();
    $em->beginTransaction();
    
    try {
      $repository = $this->getDefaultEntityManager()->getRepository('RADAR\Entity\TbRadarArea');
      /** @var TbRadarArea $entity */
      $entity = $repository->find($id);
      if ($entity) {
        $entity->setStAtivo(0);
        
        $em->persist($entity);
        $em->flush();
        
        $em->commit();
        
        return $entity;
      }
    } catch (\Exception $ex) {
      $em->rollback();
      throw $ex;
    }
  }
  
  /**
   * @param mixed $config
   */
  public function setConfig($config)
  {
    $this->config = $config;
  }
  
  public function setSrvUtil($srvUtil)
  {
    $this->srvUtil = $srvUtil;
  }
}