<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarHistAtividadeFase
 *
 * @ORM\Table(name="tb_radar_hist_atividade_fase")
 * @ORM\Entity(repositoryClass="RADAR\Repository\HistAtividadeFaseRepository")
 */
class TbRadarHistAtividadeFase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_fase", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coFase;
  
    /**
     * @var \RADAR\Entity\TbRadarAtividade
     *
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarAtividade", cascade={"persist"})
     * @ORM\JoinColumn(name="co_atividade", referencedColumnName="co_atividade", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coAtividade;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entrada", type="datetime", nullable=false)
     */
    private $dtEntrada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fechamento", type="datetime", nullable=true)
     */
    private $dtFechamento;

    
    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  
  /**
   * @return int
   */
  public function getCoFase()
  {
    return $this->coFase;
  }
  
  /**
   * @param int $coFase
   */
  public function setCoFase($coFase)
  {
    $this->coFase = $coFase;
  }
  
  /**
   * @return TbRadarAtividade
   */
  public function getCoAtividade()
  {
    return $this->coAtividade;
  }
  
  /**
   * @param TbRadarAtividade $coAtividade
   */
  public function setCoAtividade($coAtividade)
  {
    $this->coAtividade = $coAtividade;
  }
  
  /**
   * @return \DateTime
   */
  public function getDtEntrada()
  {
    return $this->dtEntrada;
  }
  
  /**
   * @param \DateTime $dtEntrada
   */
  public function setDtEntrada($dtEntrada)
  {
    $this->dtEntrada = $dtEntrada;
  }
  
  /**
   * @return \DateTime
   */
  public function getDtFechamento()
  {
    return $this->dtFechamento;
  }
  
  /**
   * @param \DateTime $dtFechamento
   */
  public function setDtFechamento($dtFechamento)
  {
    $this->dtFechamento = $dtFechamento;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
}

