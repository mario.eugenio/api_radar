<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarMembrosTime
 *
 * @ORM\Table(name="tb_radar_membros_time")
 * @ORM\Entity(repositoryClass="RADAR\Repository\MembrosTimeRepository")
 */
class TbRadarMembrosTime
{
    /**
     * @var \RADAR\Entity\TbRadarTime
     
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarTime", cascade={"persist"})
     * @ORM\JoinColumn(name="co_time", referencedColumnName="co_time", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coTime;
  
    /**
     * @var \RADAR\Entity\TbRadarPerfil
     
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarPerfil", cascade={"persist"})
     * @ORM\JoinColumn(name="co_perfil", referencedColumnName="co_perfil", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coPerfil;
  
    /**
     * @var \RADAR\Entity\TbRadarUsuario
     
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarUsuario", cascade={"persist"})
     * @ORM\JoinColumn(name="co_usuario", referencedColumnName="co_usuario", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="no_time", type="string", length=45, nullable=true)
     */
    private $noTime;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_time", type="string", length=45, nullable=true)
     */
    private $dsTime;
  
    /**
     * @var \RADAR\Entity\TbRadarArea
     
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarArea", inversedBy="usuarios", cascade={"persist"})
     * @ORM\JoinColumn(name="co_area", referencedColumnName="co_area", nullable=true)
     */
    private $coArea;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  
  /**
   * @return TbRadarTime
   */
  public function getCoTime()
  {
    return $this->coTime;
  }
  
  /**
   * @param TbRadarTime $coTime
   */
  public function setCoTime($coTime)
  {
    $this->coTime = $coTime;
  }
  
  /**
   * @return TbRadarPerfil
   */
  public function getCoPerfil()
  {
    return $this->coPerfil;
  }
  
  /**
   * @param TbRadarPerfil $coPerfil
   */
  public function setCoPerfil($coPerfil)
  {
    $this->coPerfil = $coPerfil;
  }
  
  /**
   * @return string
   */
  public function getNoTime()
  {
    return $this->noTime;
  }
  
  /**
   * @param string $noTime
   */
  public function setNoTime($noTime)
  {
    $this->noTime = $noTime;
  }
  
  /**
   * @return string
   */
  public function getDsTime()
  {
    return $this->dsTime;
  }
  
  /**
   * @param string $dsTime
   */
  public function setDsTime($dsTime)
  {
    $this->dsTime = $dsTime;
  }
  
  /**
   * @return TbRadarUsuario
   */
  public function getCoUsuario()
  {
    return $this->coUsuario;
  }
  
  /**
   * @param TbRadarUsuario $coUsuario
   */
  public function setCoUsuario($coUsuario)
  {
    $this->coUsuario = $coUsuario;
  }
  
  /**
   * @return TbRadarArea
   */
  public function getCoArea()
  {
    return $this->coArea;
  }
  
  /**
   * @param TbRadarArea $coArea
   */
  public function setCoArea($coArea)
  {
    $this->coArea = $coArea;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
}

