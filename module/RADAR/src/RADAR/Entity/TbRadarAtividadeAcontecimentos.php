<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarAtividadeAcontecimentos
 *
 * @ORM\Table(name="tb_atividade_acontecimentos")
 * @ORM\Entity(repositoryClass="RADAR\Repository\AtividadeAcontecimentosRepository")
 */
class TbRadarAtividadeAcontecimentos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_acontecimento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coAcontecimento;

    /**
     * @var integer
     *
     * @ORM\Column(name="co_atividade", type="integer", nullable=true)
     */
    private $coAtividade;

    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_acontecimento", type="datetime", nullable=true)
     */
    private $dtAcontecimento;

    /**
     * @var integer
     *
     * @ORM\Column(name="co_usuario", type="integer", nullable=true)
     */
    private $coUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_acontecimento", type="string", length=100, nullable=true)
     */
    private $dsAcontecimento;

    /**
     * @var string
     *
     * @ORM\Column(name="no_usuario", type="string", length=255, nullable=true)
     */
    private $noUsuario;
  
  /**
   * @return int
   */
  public function getCoAcontecimento()
  {
    return $this->coAcontecimento;
  }
  
  /**
   * @param int $coAcontecimento
   */
  public function setCoAcontecimento($coAcontecimento)
  {
    $this->coAcontecimento = $coAcontecimento;
  }
  
  /**
   * @return int
   */
  public function getCoAtividade()
  {
    return $this->coAtividade;
  }
  
  /**
   * @param int $coAtividade
   */
  public function setCoAtividade($coAtividade)
  {
    $this->coAtividade = $coAtividade;
  }
  
  /**
   * @return DateTime
   */
  public function getDtAcontecimento()
  {
    return $this->dtAcontecimento;
  }
  
  /**
   * @param DateTime $dtAcontecimento
   */
  public function setDtAcontecimento($dtAcontecimento)
  {
    $this->dtAcontecimento = $dtAcontecimento;
  }
  
  /**
   * @return int
   */
  public function getCoUsuario()
  {
    return $this->coUsuario;
  }
  
  /**
   * @param int $coUsuario
   */
  public function setCoUsuario($coUsuario)
  {
    $this->coUsuario = $coUsuario;
  }
  
  /**
   * @return string
   */
  public function getDsAcontecimento()
  {
    return $this->dsAcontecimento;
  }
  
  /**
   * @param string $dsAcontecimento
   */
  public function setDsAcontecimento($dsAcontecimento)
  {
    $this->dsAcontecimento = $dsAcontecimento;
  }
  
  /**
   * @return string
   */
  public function getNoUsuario()
  {
    return $this->noUsuario;
  }
  
  /**
   * @param string $noUsuario
   */
  public function setNoUsuario($noUsuario)
  {
    $this->noUsuario = $noUsuario;
  }
  
}

