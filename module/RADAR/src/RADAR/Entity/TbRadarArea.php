<?php

namespace RADAR\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * TbRadarArea
 *
 * @ORM\Table(name="tb_radar_area")
 * @ORM\Entity(repositoryClass="RADAR\Repository\AreaRepository")
 */
class TbRadarArea extends \AbstractEntity
{
  public function __construct()
  {
    $this->usuarios = new ArrayCollection();
  }
  
  
  /**
     * @var integer
     *
     * @ORM\Column(name="co_area", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coArea;

    /**
     * @var string
     *
     * @ORM\Column(name="no_area", type="string", length=45, nullable=true)
     */
    private $noArea;
  
    /**
     * @var string
     *
     * @ORM\Column(name="sg_area", type="string", length=45, nullable=true)
     */
    private $sgArea;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_area", type="string", length=45, nullable=true)
     */
    private $dsArea;
  
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_criacao", type="datetime", nullable=true)
     */
    private $dtCriacao;
    
    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  
  /**
   * @ORM\OneToMany(targetEntity="\RADAR\Entity\TbRadarMembrosTime", mappedBy="coArea")
   * @ORM\JoinColumn(nullable=false)
   */
  protected $usuarios;
  
  /**
   * @return int
   */
  public function getCoArea()
  {
    return $this->coArea;
  }
  
  /**
   * @param int $coArea
   */
  public function setCoArea($coArea)
  {
    $this->coArea = $coArea;
  }
  
  /**
   * @return string
   */
  public function getNoArea()
  {
    return $this->noArea;
  }
  
  /**
   * @param string $noArea
   */
  public function setNoArea($noArea)
  {
    $this->noArea = $noArea;
  }
  
  /**
   * @return string
   */
  public function getDsArea()
  {
    return $this->dsArea;
  }
  
  /**
   * @param string $dsArea
   */
  public function setDsArea($dsArea)
  {
    $this->dsArea = $dsArea;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
  
  /**
   * @return string
   */
  public function getSgArea()
  {
    return $this->sgArea;
  }
  
  /**
   * @param string $sgArea
   */
  public function setSgArea($sgArea)
  {
    $this->sgArea = $sgArea;
  }
  
  /**
   * @return \DateTime
   */
  public function getDtCriacao()
  {
    return $this->dtCriacao;
  }
  
  /**
   * @param \DateTime $dtCriacao
   */
  public function setDtCriacao($dtCriacao)
  {
    $this->dtCriacao = $dtCriacao;
  }
  
  /**
   * @return mixed
   */
  public function getUsuarios()
  {
    return $this->usuarios;
  }
  
  
}

