<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarEnvolvido
 *
 * @ORM\Table(name="tb_radar_envolvido")
 * @ORM\Entity(repositoryClass="RADAR\Repository\EnvolvidoRepository")
 */
class TbRadarEnvolvido
{
  /**
   * @var \RADAR\Entity\TbRadarUsuario
   
   * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarUsuario", cascade={"persist"})
   * @ORM\JoinColumn(name="co_envolvido", referencedColumnName="co_usuario", nullable=true)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   */
  private $coEnvolvido;
  
  /**
   * @var integer
   *
   * @ORM\Column(name="co_atividade", type="integer", nullable=true)
   */
  private $coAtividade;
  
  /**
   * @return TbRadarUsuario
   */
  public function getCoEnvolvido()
  {
    return $this->coEnvolvido;
  }
  
  /**
   * @param TbRadarUsuario $coEnvolvido
   */
  public function setCoEnvolvido($coEnvolvido)
  {
    $this->coEnvolvido = $coEnvolvido;
  }
  
  /**
   * @return TbRadarAtividade
   */
  public function getCoAtividade()
  {
    return $this->coAtividade;
  }
  
  /**
   * @param TbRadarAtividade $coAtividade
   */
  public function setCoAtividade($coAtividade)
  {
    $this->coAtividade = $coAtividade;
  }
}

