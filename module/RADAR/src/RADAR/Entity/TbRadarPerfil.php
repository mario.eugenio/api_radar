<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarPerfil
 *
 * @ORM\Table(name="tb_radar_perfil")
 * @ORM\Entity(repositoryClass="RADAR\Repository\PerfilRepository")
 */
class TbRadarPerfil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_perfil", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coPerfil;

    /**
     * @var string
     *
     * @ORM\Column(name="no_perfil", type="string", length=45, nullable=true)
     */
    private $noPerfil;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_perfil", type="string", length=45, nullable=true)
     */
    private $dsPerfil;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  
  /**
   * @return int
   */
  public function getCoPerfil()
  {
    return $this->coPerfil;
  }
  
  /**
   * @param int $coPerfil
   */
  public function setCoPerfil($coPerfil)
  {
    $this->coPerfil = $coPerfil;
  }
  
  /**
   * @return string
   */
  public function getNoPerfil()
  {
    return $this->noPerfil;
  }
  
  /**
   * @param string $noPerfil
   */
  public function setNoPerfil($noPerfil)
  {
    $this->noPerfil = $noPerfil;
  }
  
  /**
   * @return string
   */
  public function getDsPerfil()
  {
    return $this->dsPerfil;
  }
  
  /**
   * @param string $dsPerfil
   */
  public function setDsPerfil($dsPerfil)
  {
    $this->dsPerfil = $dsPerfil;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
}

