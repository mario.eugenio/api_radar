<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarFuncionalidade
 *
 * @ORM\Table(name="tb_radar_funcionalidade")
 * @ORM\Entity(repositoryClass="RADAR\Repository\FuncionalidadeRepository")
 */
class TbRadarFuncionalidade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_funcionalidade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coFuncionalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="no_funcionalidade", type="string", length=45, nullable=true)
     */
    private $noFuncionalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_funcionalidade", type="string", length=45, nullable=true)
     */
    private $dsFuncionalidade;
  
    /**
     * @var integer
     *
     * @ORM\Column(name="co_acao", type="integer", nullable=true)
     */
    private $coAcao;

    /**
     * @var string
     *
     * @ORM\Column(name="perfil", type="string", length=45, nullable=true)
     */
    private $perfil;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=true)
     */
    private $stAtivo = '1';
  
  /**
   * @return int
   */
  public function getCoFuncionalidade()
  {
    return $this->coFuncionalidade;
  }
  
  /**
   * @param int $coFuncionalidade
   */
  public function setCoFuncionalidade($coFuncionalidade)
  {
    $this->coFuncionalidade = $coFuncionalidade;
  }
  
  /**
   * @return string
   */
  public function getNoFuncionalidade()
  {
    return $this->noFuncionalidade;
  }
  
  /**
   * @param string $noFuncionalidade
   */
  public function setNoFuncionalidade($noFuncionalidade)
  {
    $this->noFuncionalidade = $noFuncionalidade;
  }
  
  /**
   * @return string
   */
  public function getDsFuncionalidade()
  {
    return $this->dsFuncionalidade;
  }
  
  /**
   * @param string $dsFuncionalidade
   */
  public function setDsFuncionalidade($dsFuncionalidade)
  {
    $this->dsFuncionalidade = $dsFuncionalidade;
  }
  
  /**
   * @return TbRadarAcao
   */
  public function getCoAcao()
  {
    return $this->coAcao;
  }
  
  /**
   * @param TbRadarAcao $coAcao
   */
  public function setCoAcao($coAcao)
  {
    $this->coAcao = $coAcao;
  }
  
  /**
   * @return TbRadarPerfil
   */
  public function getPerfil()
  {
    return $this->perfil;
  }
  
  /**
   * @param TbRadarPerfil $perfil
   */
  public function setPerfil($perfil)
  {
    $this->perfil = $perfil;
  }
  
  /**
   * @return bool
   */
  public function isStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param bool $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
}

