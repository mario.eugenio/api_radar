<?php

namespace RADAR\Entity;

use Application\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbAtividadeComentarios
 *
 * @ORM\Table(name="tb_atividade_comentarios")
 * @ORM\Entity(repositoryClass="RADAR\Repository\AtividadeComentariosRepository")
 */
class TbAtividadeComentarios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_comentario", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coComentario;

    /**
     * @var \RADAR\Entity\TbRadarAtividade
     *
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarAtividade", cascade={"persist"})
     * @ORM\JoinColumn(name="co_atividade", referencedColumnName="co_atividade", nullable=true)
     */
    private $coAtividade;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="co_usuario_comentador", type="integer", nullable=true)
     */
    private $coUsuarioComentador;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_comentario", type="datetime", nullable=true)
     */
    private $dtComentario;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_comentario", type="string", length=255, nullable=true)
     */
    private $dsComentario;
  
  /**
   * @return int
   */
  public function getCoComentario()
  {
    return $this->coComentario;
  }
  
  /**
   * @param int $coComentario
   */
  public function setCoComentario($coComentario)
  {
    $this->coComentario = $coComentario;
  }
  
  /**
   * @return TbRadarAtividade
   */
  public function getCoAtividade()
  {
    return $this->coAtividade;
  }
  
  /**
   * @param TbRadarAtividade $coAtividade
   */
  public function setCoAtividade($coAtividade)
  {
    $this->coAtividade = $coAtividade;
  }
  
  /**
   * @return int
   */
  public function getCoUsuarioComentador()
  {
    return $this->coUsuarioComentador;
  }
  
  /**
   * @param int $coUsuarioComentador
   */
  public function setCoUsuarioComentador($coUsuarioComentador)
  {
    $this->coUsuarioComentador = $coUsuarioComentador;
  }
  
  /**
   * @return \DateTime
   */
  public function getDtComentario()
  {
    return $this->dtComentario;
  }
  
  /**
   * @param \DateTime $dtComentario
   */
  public function setDtComentario($dtComentario)
  {
    $this->dtComentario = $dtComentario;
  }
  
  /**
   * @return string
   */
  public function getDsComentario()
  {
    return $this->dsComentario;
  }
  
  /**
   * @param string $dsComentario
   */
  public function setDsComentario($dsComentario)
  {
    $this->dsComentario = $dsComentario;
  }
}

