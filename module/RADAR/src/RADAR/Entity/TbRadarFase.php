<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarFase
 *
 * @ORM\Table(name="tb_radar_fase")
 * @ORM\Entity(repositoryClass="RADAR\Repository\FaseRepository")
 */
class TbRadarFase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_fase", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coFase;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_fase", type="string", length=45, nullable=true)
     */
    private $dsFase;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  
  /**
   * @return int
   */
  public function getCoFase()
  {
    return $this->coFase;
  }
  
  /**
   * @param int $coFase
   */
  public function setCoFase($coFase)
  {
    $this->coFase = $coFase;
  }
  
  /**
   * @return string
   */
  public function getDsFase()
  {
    return $this->dsFase;
  }
  
  /**
   * @param string $dsFase
   */
  public function setDsFase($dsFase)
  {
    $this->dsFase = $dsFase;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
}

