<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation;

/**
 * TbRadarUsuario
 *
 * @ORM\Table(name="tb_radar_usuario")
 * @ORM\Entity(repositoryClass="RADAR\Repository\UsuarioRepository")
 */
class TbRadarUsuario extends \AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_usuario", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $coUsuario;
  

    /**
     * @var string
     *
     * @ORM\Column(name="no_usuario", type="string", length=45, nullable=true)
     */
    private $noUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_usuario", type="string", length=45, nullable=true)
     */
    private $telUsuario;
  
  /**
   * @var \RADAR\Entity\TbRadarPerfil
   
   * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarPerfil", cascade={"persist"})
   * @ORM\JoinColumn(name="co_perfil", referencedColumnName="co_perfil", nullable=false)
   */
    private $perfil;

    /**
     * @var string
     *
     * @ORM\Column(name="email_usuario", type="string", length=45, nullable=true)
     */
    private $emailUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="senha", type="string", length=45, nullable=true)
     */
    private $senha;
    
    private $repitaSenha;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;

    /**
     * @var string
     *
     * @ORM\Column(name="imagem_usuario", type="text", nullable=true)
     */
     private $imagemUsuario;
  
  /**
   * @return int
   */
  public function getCoUsuario()
  {
    return $this->coUsuario;
  }
  
  /**
   * @param int $coUsuario
   */
  public function setCoUsuario($coUsuario)
  {
    $this->coUsuario = $coUsuario;
  }
  
  /**
   * @return string
   */
  public function getNoUsuario()
  {
    return $this->noUsuario;
  }
  
  /**
   * @param string $noUsuario
   */
  public function setNoUsuario($noUsuario)
  {
    $this->noUsuario = $noUsuario;
  }
  
  /**
   * @return string
   */
  public function getTelUsuario()
  {
    return $this->telUsuario;
  }
  
  /**
   * @param string $telUsuario
   */
  public function setTelUsuario($telUsuario)
  {
    $this->telUsuario = $telUsuario;
  }
  
  /**
   * @return TbRadarPerfil
   */
  public function getPerfil()
  {
    return $this->perfil;
  }
  
  /**
   * @param TbRadarPerfil $perfil
   */
  public function setPerfil($perfil)
  {
    $this->perfil = $perfil;
  }
  
  /**
   * @return string
   */
  public function getEmailUsuario()
  {
    return $this->emailUsuario;
  }
  
  /**
   * @param string $emailUsuario
   */
  public function setEmailUsuario($emailUsuario)
  {
    $this->emailUsuario = $emailUsuario;
  }
  
  /**
   * @return string
   */
  public function getSenha()
  {
    return $this->senha;
  }
  
  /**
   * @param string $senha
   */
  public function setSenha($senha)
  {
    $this->senha = $senha;
  }
  
  /**
   * @return mixed
   */
  public function getRepitaSenha()
  {
    return $this->repitaSenha;
  }
  
  /**
   * @param mixed $repitaSenha
   */
  public function setRepitaSenha($repitaSenha)
  {
    $this->repitaSenha = $repitaSenha;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
  
  /**
   * @return string
   */
  public function getImagemUsuario()
  {
    return $this->imagemUsuario;
  }
  
  /**
   * @param string $imagemUsuario
   */
  public function setImagemUsuario($imagemUsuario)
  {
    $this->imagemUsuario = $imagemUsuario;
  }
}


