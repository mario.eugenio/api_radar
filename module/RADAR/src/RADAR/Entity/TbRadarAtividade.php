<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * TbRadarAtividade
 *
 * @ORM\Table(name="tb_radar_atividade")
 * @ORM\Entity(repositoryClass="RADAR\Repository\AtividadeRepository")
 */
class TbRadarAtividade extends \AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_atividade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coAtividade;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_atividade", type="string", length=45, nullable=true)
     */
    private $nomeAtividade;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entrega", type="datetime", nullable=true)
     */
    private $dtEntrega;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_atividade", type="string", length=45, nullable=true)
     */
    private $dsAtividade;

    /**
     * @var string
     *
     * @ORM\Column(name="no_area", type="string", length=45, nullable=true)
     */
    private $noArea;
  
    /**
     * @var \RADAR\Entity\TbRadarUsuario
     
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarUsuario", cascade={"persist"})
     * @ORM\JoinColumn(name="co_usuario_executor", referencedColumnName="co_usuario", nullable=true)
     */
    private $coUsuarioExecutor;
  
    /**
     * @var \RADAR\Entity\TbRadarUsuario
     
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarUsuario", cascade={"persist"})
     * @ORM\JoinColumn(name="co_usuario_demandante", referencedColumnName="co_usuario", nullable=true)
     */
    private $coUsuarioDemandante;
  
    /**
     * @var \RADAR\Entity\TbRadarUsuario
     
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarUsuario", cascade={"persist"})
     * @ORM\JoinColumn(name="co_usuario_envolvido", referencedColumnName="co_usuario", nullable=true)
     */
    private $coUsuarioEnvolvido;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="co_anexo", type="integer", nullable=true)
     */
    private $coAnexo;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  
    /**
     * @var \RADAR\Entity\TbRadarArea
     
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarArea", cascade={"persist"})
     * @ORM\JoinColumn(name="co_area", referencedColumnName="co_area", nullable=true)
     */
    private $coArea;
  
    /**
     * @OneToMany(targetEntity="\RADAR\Entity\TbAtividadeComentarios", mappedBy="coAtividade")
     */
    protected $comentarios;
  
    /**
     * @OneToMany(targetEntity="\RADAR\Entity\TbRadarAtividadeAnexos", mappedBy="coAtividade")
     */
    protected $anexo;
  
    /**
     * @OneToMany(targetEntity="\RADAR\Entity\TbRadarAtividadeAcontecimentos", mappedBy="coAtividade")
     */
    protected $acontecimentos;
  
    /**
     * @OneToMany(targetEntity="\RADAR\Entity\TbRadarHistAtividadeFase", mappedBy="coAtividade")
     */
    protected $historicoFase;
  
    /**
     * Many Users have Many Groups.
     * @ManyToMany(targetEntity="\RADAR\Entity\TbRadarUsuario")
     * @JoinTable(name="tb_radar_atividade_envolvido",
     *      joinColumns={@JoinColumn(name="co_atividade", referencedColumnName="co_atividade")},
     *      inverseJoinColumns={@JoinColumn(name="co_envolvido", referencedColumnName="co_usuario")}
     *      )
     */
    protected $envolvidos;
    
  /**
   * @return int
   */
  public function getCoAtividade()
  {
    return $this->coAtividade;
  }
  
  /**
   * @param int $coAtividade
   */
  public function setCoAtividade($coAtividade)
  {
    $this->coAtividade = $coAtividade;
  }
  
  /**
   * @return string
   */
  public function getNomeAtividade()
  {
    return $this->nomeAtividade;
  }
  
  /**
   * @param string $nomeAtividade
   */
  public function setNomeAtividade($nomeAtividade)
  {
    $this->nomeAtividade = $nomeAtividade;
  }
  
  /**
   * @return \DateTime
   */
  public function getDtEntrega()
  {
    return $this->dtEntrega;
  }
  
  /**
   * @param \DateTime $dtEntrega
   */
  public function setDtEntrega($dtEntrega)
  {
    $this->dtEntrega = $dtEntrega;
  }
  
  /**
   * @return string
   */
  public function getDsAtividade()
  {
    return $this->dsAtividade;
  }
  
  /**
   * @param string $dsAtividade
   */
  public function setDsAtividade($dsAtividade)
  {
    $this->dsAtividade = $dsAtividade;
  }
  
  /**
   * @return string
   */
  public function getNoArea()
  {
    return $this->noArea;
  }
  
  /**
   * @param string $noArea
   */
  public function setNoArea($noArea)
  {
    $this->noArea = $noArea;
  }
  
  /**
   * @return TbRadarUsuario
   */
  public function getCoUsuarioExecutor()
  {
    return $this->coUsuarioExecutor;
  }
  
  /**
   * @param TbRadarUsuario $coUsuarioExecutor
   */
  public function setCoUsuarioExecutor($coUsuarioExecutor)
  {
    $this->coUsuarioExecutor = $coUsuarioExecutor;
  }
  
  /**
   * @return TbRadarUsuario
   */
  public function getCoUsuarioDemandante()
  {
    return $this->coUsuarioDemandante;
  }
  
  /**
   * @param TbRadarUsuario $coUsuarioDemandante
   */
  public function setCoUsuarioDemandante($coUsuarioDemandante)
  {
    $this->coUsuarioDemandante = $coUsuarioDemandante;
  }
  
  /**
   * @return TbRadarUsuario
   */
  public function getCoUsuarioEnvolvido()
  {
    return $this->coUsuarioEnvolvido;
  }
  
  /**
   * @param TbRadarUsuario $coUsuarioEnvolvido
   */
  public function setCoUsuarioEnvolvido($coUsuarioEnvolvido)
  {
    $this->coUsuarioEnvolvido = $coUsuarioEnvolvido;
  }
  
  /**
   * @return int
   */
  public function getCoAnexo()
  {
    return $this->coAnexo;
  }
  
  /**
   * @param int $coAnexo
   */
  public function setCoAnexo($coAnexo)
  {
    $this->coAnexo = $coAnexo;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
  
  /**
   * @return TbRadarArea
   */
  public function getCoArea()
  {
    return $this->coArea;
  }
  
  /**
   * @param TbRadarArea $coArea
   */
  public function setCoArea($coArea)
  {
    $this->coArea = $coArea;
  }
  
  /**
   * @return mixed
   */
  public function getComentarios()
  {
    return $this->comentarios;
  }
  
  /**
   * @param mixed $comentarios
   */
  public function setComentarios($comentarios)
  {
    $this->comentarios = $comentarios;
  }
  
}

