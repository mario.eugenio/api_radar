<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarAcao
 *
 * @ORM\Table(name="tb_radar_acao")
 * @ORM\Entity(repositoryClass="RADAR\Repository\AcaoRepository")
 */
class TbRadarAcao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_acao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coAcao;

    /**
     * @var integer
     *
     * @ORM\Column(name="co_funcionalidade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coFuncionalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="no_acao", type="string", length=45, nullable=true)
     */
    private $noAcao;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_acao", type="string", length=45, nullable=true)
     */
    private $dsAcao;

    /**
     * @var string
     *
     * @ORM\Column(name="co_perfil", type="string", length=45, nullable=true)
     */
    private $coPerfil;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  
  /**
   * @return int
   */
  public function getCoAcao()
  {
    return $this->coAcao;
  }
  
  /**
   * @param int $coAcao
   */
  public function setCoAcao($coAcao)
  {
    $this->coAcao = $coAcao;
  }
  
  /**
   * @return int
   */
  public function getCoFuncionalidade()
  {
    return $this->coFuncionalidade;
  }
  
  /**
   * @param int $coFuncionalidade
   */
  public function setCoFuncionalidade($coFuncionalidade)
  {
    $this->coFuncionalidade = $coFuncionalidade;
  }
  
  /**
   * @return string
   */
  public function getNoAcao()
  {
    return $this->noAcao;
  }
  
  /**
   * @param string $noAcao
   */
  public function setNoAcao($noAcao)
  {
    $this->noAcao = $noAcao;
  }
  
  /**
   * @return string
   */
  public function getDsAcao()
  {
    return $this->dsAcao;
  }
  
  /**
   * @param string $dsAcao
   */
  public function setDsAcao($dsAcao)
  {
    $this->dsAcao = $dsAcao;
  }
  
  /**
   * @return string
   */
  public function getCoPerfil()
  {
    return $this->coPerfil;
  }
  
  /**
   * @param string $coPerfil
   */
  public function setCoPerfil($coPerfil)
  {
    $this->coPerfil = $coPerfil;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
}

