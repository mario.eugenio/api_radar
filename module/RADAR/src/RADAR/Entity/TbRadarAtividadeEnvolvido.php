<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarAtividadeEnvolvido
 *
 * @ORM\Table(name="tb_radar_atividade_envolvido")
 * @ORM\Entity(repositoryClass="RADAR\Repository\AtividadeEnvolvidoRepository")
 */
class TbRadarAtividadeEnvolvido
{
  /**
   * @var integer
   *
   * @ORM\Column(name="co_envolvido", type="integer", nullable=true)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   */
    private $coEnvolvido;
  
  /**
   * @var integer
   *
   * @ORM\Column(name="co_atividade", type="integer", nullable=true)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="NONE")
   */
  private $coAtividade;
  
  /**
   * @return integer
   */
  public function getCoEnvolvido()
  {
    return $this->coEnvolvido;
  }
  
  /**
   * @param integer $coEnvolvido
   */
  public function setCoEnvolvido($coEnvolvido)
  {
    $this->coEnvolvido = $coEnvolvido;
  }
  
  /**
   * @return integer
   */
  public function getCoAtividade()
  {
    return $this->coAtividade;
  }
  
  /**
   * @param integer $coAtividade
   */
  public function setCoAtividade($coAtividade)
  {
    $this->coAtividade = $coAtividade;
  }
  
}

