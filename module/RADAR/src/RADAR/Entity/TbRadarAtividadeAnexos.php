<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarAtividadeAnexos
 *
 * @ORM\Table(name="tb_radar_atividade_anexos")
 * @ORM\Entity(repositoryClass="RADAR\Repository\AtividadeAnexosRepository")
 */
class TbRadarAtividadeAnexos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_anexo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coAnexo;
  
    /**
     * @var \RADAR\Entity\TbRadarAtividade
     *
     * @ORM\ManyToOne(targetEntity="\RADAR\Entity\TbRadarAtividade", cascade={"persist"})
     * @ORM\JoinColumn(name="co_atividade", referencedColumnName="co_atividade", nullable=true)
     */
    private $coAtividade;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_caminho", type="string", length=45, nullable=true)
     */
    private $dsCaminho;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  

    /**
     * @var string
     *
     * @ORM\Column(name="no_anexo", type="string", length=100, nullable=true)
     */
    private $noAnexo;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_tamanho", type="string", length=50, nullable=true)
     */
    private $nuTamanho;
  
  /**
   * @return int
   */
  public function getCoAnexo()
  {
    return $this->coAnexo;
  }
  
  /**
   * @param int $coAnexo
   */
  public function setCoAnexo($coAnexo)
  {
    $this->coAnexo = $coAnexo;
  }
  
  /**
   * @return TbRadarAtividade
   */
  public function getCoAtividade()
  {
    return $this->coAtividade;
  }
  
  /**
   * @param TbRadarAtividade $coAtividade
   */
  public function setCoAtividade($coAtividade)
  {
    $this->coAtividade = $coAtividade;
  }
  
  /**
   * @return string
   */
  public function getDsCaminho()
  {
    return $this->dsCaminho;
  }
  
  /**
   * @param string $dsCaminho
   */
  public function setDsCaminho($dsCaminho)
  {
    $this->dsCaminho = $dsCaminho;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
  
  /**
   * @return string
   */
  public function getNoAnexo()
  {
    return $this->noAnexo;
  }
  
  /**
   * @param string $noAnexo
   */
  public function setNoAnexo($noAnexo)
  {
    $this->noAnexo = $noAnexo;
  }
  
  /**
   * @return string
   */
  public function getNuTamanho()
  {
    return $this->nuTamanho;
  }
  
  /**
   * @param string $nuTamanho
   */
  public function setNuTamanho($nuTamanho)
  {
    $this->nuTamanho = $nuTamanho;
  }
}

