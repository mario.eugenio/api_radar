<?php

namespace RADAR\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbRadarTime
 *
 * @ORM\Table(name="tb_radar_time")
 * @ORM\Entity(repositoryClass="RADAR\Repository\TimeRepository")
 */
class TbRadarTime
{
    /**
     * @var integer
     *
     * @ORM\Column(name="co_time", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $coTime;

    /**
     * @var string
     *
     * @ORM\Column(name="no_time", type="string", length=45, nullable=true)
     */
    private $noTime;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_time", type="string", length=45, nullable=true)
     */
    private $dsTime;

    /**
     * @var string
     *
     * @ORM\Column(name="st_ativo", type="string", length=45, nullable=true)
     */
    private $stAtivo;
  
  /**
   * @return int
   */
  public function getCoTime()
  {
    return $this->coTime;
  }
  
  /**
   * @param int $coTime
   */
  public function setCoTime($coTime)
  {
    $this->coTime = $coTime;
  }
  
  /**
   * @return string
   */
  public function getNoTime()
  {
    return $this->noTime;
  }
  
  /**
   * @param string $noTime
   */
  public function setNoTime($noTime)
  {
    $this->noTime = $noTime;
  }
  
  /**
   * @return string
   */
  public function getDsTime()
  {
    return $this->dsTime;
  }
  
  /**
   * @param string $dsTime
   */
  public function setDsTime($dsTime)
  {
    $this->dsTime = $dsTime;
  }
  
  /**
   * @return string
   */
  public function getStAtivo()
  {
    return $this->stAtivo;
  }
  
  /**
   * @param string $stAtivo
   */
  public function setStAtivo($stAtivo)
  {
    $this->stAtivo = $stAtivo;
  }
}

