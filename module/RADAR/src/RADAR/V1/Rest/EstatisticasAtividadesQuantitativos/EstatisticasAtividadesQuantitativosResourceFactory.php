<?php
namespace RADAR\V1\Rest\EstatisticasAtividadesQuantitativos;

class EstatisticasAtividadesQuantitativosResourceFactory
{
    public function __invoke($services)
    {
      $resource = new EstatisticasAtividadesQuantitativosResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
