<?php
namespace RADAR\V1\Rest\Atividades;

use RADAR\Service\AtividadeService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AtividadesResource extends AbstractResourceListener
{
  /** @var  AtividadeService */
  public $service;
  
  /**
   * Create a resource
   *
   * @param  mixed $data
   * @return ApiProblem|mixed
   */
  public function create($data)
  {
    try {
      $result = $this->service->save($data);
      
      return $result;
    } catch (ValidationException $ex) {
      return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param AtividadeService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
