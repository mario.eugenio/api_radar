<?php
namespace RADAR\V1\Rest\Atividades;

class AtividadesResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
