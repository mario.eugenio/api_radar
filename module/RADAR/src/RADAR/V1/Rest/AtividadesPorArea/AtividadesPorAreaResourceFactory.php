<?php
namespace RADAR\V1\Rest\AtividadesPorArea;

class AtividadesPorAreaResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesPorAreaResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
