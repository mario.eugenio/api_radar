<?php
namespace RADAR\V1\Rest\AtividadesEstatisticas;

use RADAR\Service\AtividadeService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AtividadesEstatisticasResource extends AbstractResourceListener
{
  /** @var  AtividadeService */
  public $service;
  
  
  /**
   * @param AtividadeService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
