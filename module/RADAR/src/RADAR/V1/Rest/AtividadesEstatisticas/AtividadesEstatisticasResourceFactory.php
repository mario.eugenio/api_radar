<?php
namespace RADAR\V1\Rest\AtividadesEstatisticas;

class AtividadesEstatisticasResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesEstatisticasResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
