<?php
namespace RADAR\V1\Rest\UsuariosInativos;

use RADAR\Service\UsuarioService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class UsuariosInativosResource extends AbstractResourceListener
{
  /** @var  UsuarioService */
  public $service;
  
    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
      try {
    
        $result = $this->service->getUsuarioInativos();
        return $result;
      } catch (\Exception $e) {
        return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
      }
    }
  
  
  /**
   * @param UsuarioService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
