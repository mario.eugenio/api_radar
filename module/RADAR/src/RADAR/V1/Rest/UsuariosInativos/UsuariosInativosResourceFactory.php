<?php
namespace RADAR\V1\Rest\UsuariosInativos;

class UsuariosInativosResourceFactory
{
    public function __invoke($services)
    {
        $resource = new UsuariosInativosResource();
        $resource->setService($services->get('UsuarioService'));
        return $resource;
    }
}
