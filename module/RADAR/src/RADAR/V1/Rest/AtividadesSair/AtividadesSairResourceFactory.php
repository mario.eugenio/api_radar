<?php
namespace RADAR\V1\Rest\AtividadesSair;

class AtividadesSairResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesSairResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
