<?php
namespace RADAR\V1\Rest\EstatisticasAtividades;

use RADAR\Service\AtividadeService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class EstatisticasAtividadesResource extends AbstractResourceListener
{
  /** @var  AtividadeService */
  public $service;
  
  /**
   * Fetch a resource
   *
   * @param  mixed $id
   * @return ApiProblem|mixed
   */
  public function fetch($id)
  {
    try {
      $result = $this->service->estatisticasAtividades($id);
      return $result;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  
  /**
   * @param PerfilService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
