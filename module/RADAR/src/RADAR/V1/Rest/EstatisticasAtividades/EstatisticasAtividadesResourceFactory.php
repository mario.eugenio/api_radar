<?php
namespace RADAR\V1\Rest\EstatisticasAtividades;

class EstatisticasAtividadesResourceFactory
{
    public function __invoke($services)
    {
      $resource = new EstatisticasAtividadesResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
