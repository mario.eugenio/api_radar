<?php
namespace RADAR\V1\Rest\Usuarios;

class UsuariosResourceFactory
{
    public function __invoke($services)
    {
      $resource = new UsuariosResource();
      $resource->setService($services->get('UsuarioService'));
      return $resource;
    }
}
