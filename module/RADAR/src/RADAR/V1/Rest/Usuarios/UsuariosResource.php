<?php
namespace RADAR\V1\Rest\Usuarios;

use Application\Exception\ValidationException;
use Application\Resource\AbstractResource;
use Doctrine\Common\Util\Debug;
use Herrera\Json\Exception\Exception;
use RADAR\Service\UsuarioService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class UsuariosResource extends AbstractResource
{
    /** @var  UsuarioService */
    public $service;
  
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
      try {
        $result = $this->service->save($data);
        return $result;
      } catch (ValidationException $ex) {
        return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
      } catch (\Exception $e) {
        return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
      }
    }
    
    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
      try {
        $result = $this->service->getUsuario($id);
        //Debug::dump($result); exit;
        return $result;
      } catch (\Exception $e) {
        return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
      }
    }
    
    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
      try {
        $result = $this->service->save($data, $id);
        return $result;
      } catch (ValidationException $ex) {
        return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
      } catch (\Exception $e) {
        return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
      }
    }
    
    /**
     * @param array $params
     * @return array|ApiProblem
     */
    public function fetchAll($params = [])
    {
      try {
        $arrParams = $params->toArray();
        if(!isset($arrParams['search'])) {
          throw new \Exception('Digite algum texto para pesquisa');
        }
        
        $result = $this->service->search($arrParams['search']);
        return $result;
      } catch (\Exception $e) {
        return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
      }
    }
  
    /**
     * Delete a resource
     *
     * @param mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
      try {
        $result = $this->service->inativarUsuario($id);
        return $result;
      } catch (\Exception $e) {
        return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
      }
    }
    
    /**
     * @param UsuarioService $service
     * @return $this
     */
    public function setService($service)
    {
      $this->service = $service;
      return $this;
    }
}
