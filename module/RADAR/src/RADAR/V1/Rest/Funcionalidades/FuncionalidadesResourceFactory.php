<?php
namespace RADAR\V1\Rest\Funcionalidades;

class FuncionalidadesResourceFactory
{
    public function __invoke($services)
    {
      $resource = new FuncionalidadesResource();
      $resource->setService($services->get('PerfilService'));
  
      return $resource;
    }
}
