<?php
namespace RADAR\V1\Rest\AtividadesExecutar;

class AtividadesExecutarResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesExecutarResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
