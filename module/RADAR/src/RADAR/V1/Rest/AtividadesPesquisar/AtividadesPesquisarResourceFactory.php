<?php
namespace RADAR\V1\Rest\AtividadesPesquisar;

class AtividadesPesquisarResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesPesquisarResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
