<?php
namespace RADAR\V1\Rest\AtividadesPendentes;

class AtividadesPendentesResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesPendentesResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
