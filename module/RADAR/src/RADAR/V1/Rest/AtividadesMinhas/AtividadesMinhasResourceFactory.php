<?php
namespace RADAR\V1\Rest\AtividadesMinhas;

class AtividadesMinhasResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesMinhasResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
