<?php
namespace RADAR\V1\Rest\AtividadesMinhas;

use RADAR\Service\AtividadeService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AtividadesMinhasResource extends AbstractResourceListener
{
  /** @var  AtividadeService */
  public $service;
  
  /**
   * @param array $params
   * @return array|ApiProblem
   */
  public function fetchAll($params = [])
  {
    try {
      $arrParams = $params->toArray();
      
      if(!isset($arrParams['idArea'])) {
        throw new \Exception('Área não encontrada');
      }
      
      if(!isset($arrParams['idUsuario'])) {
        throw new \Exception('Usuário não encontrado');
      }
      
      $result = $this->service->listarMinhasAtividades($arrParams['idArea'], $arrParams['idUsuario']);
      return $result;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param AtividadeService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
