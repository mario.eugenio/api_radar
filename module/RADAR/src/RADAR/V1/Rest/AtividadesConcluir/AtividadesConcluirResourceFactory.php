<?php
namespace RADAR\V1\Rest\AtividadesConcluir;

class AtividadesConcluirResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesConcluirResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
