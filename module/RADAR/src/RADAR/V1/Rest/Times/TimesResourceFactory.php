<?php
namespace RADAR\V1\Rest\Times;

class TimesResourceFactory
{
    public function __invoke($services)
    {
      $resource = new TimesResource();
      $resource->setService($services->get('TimeService'));
      return $resource;
    }
}
