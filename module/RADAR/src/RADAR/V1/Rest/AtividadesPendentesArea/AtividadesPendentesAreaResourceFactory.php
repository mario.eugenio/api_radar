<?php
namespace RADAR\V1\Rest\AtividadesPendentesArea;

class AtividadesPendentesAreaResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesPendentesAreaResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
