<?php
namespace RADAR\V1\Rest\AtividadesPendentesArea;

use RADAR\Service\AtividadeService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AtividadesPendentesAreaResource extends AbstractResourceListener
{
  /** @var  AtividadeService */
  public $service;
  
  /**
   * @param array $params
   * @return array|ApiProblem
   */
  public function fetchAll($params = [])
  {
    try {
      $arrParams = $params->toArray();
      
      if(!isset($arrParams['idArea'])) {
        throw new \Exception('Área não encontrada');
      }
      
      $result = $this->service->listarAtividadesPendentesPorArea($arrParams['idArea']);
      return $result;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param AtividadeService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
