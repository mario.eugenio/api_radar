<?php
namespace RADAR\V1\Rest\AtividadesHistorico;

use RADAR\Service\AtividadeService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AtividadesHistoricoResource extends AbstractResourceListener
{
  /** @var  AtividadeService */
  public $service;
  
  /**
   * @param array $params
   * @return array|ApiProblem
   */
  public function fetchAll($params = [])
  {
    try {
      $arrParams = $params->toArray();
      
      if(!isset($arrParams['month'])) {
        throw new \Exception('Mês não encontrado');
      }
      
      if(!isset($arrParams['year'])) {
        throw new \Exception('Ano não encontrado');
      }
      
      $result = $this->service->historicoAtividades($arrParams['month'], $arrParams['year']);
      return $result;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param AtividadeService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
