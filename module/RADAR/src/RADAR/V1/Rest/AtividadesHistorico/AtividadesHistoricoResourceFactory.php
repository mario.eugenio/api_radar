<?php
namespace RADAR\V1\Rest\AtividadesHistorico;

class AtividadesHistoricoResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesHistoricoResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
