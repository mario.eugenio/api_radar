<?php
namespace RADAR\V1\Rest\AtividadesArea;

class AtividadesAreaResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesAreaResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
