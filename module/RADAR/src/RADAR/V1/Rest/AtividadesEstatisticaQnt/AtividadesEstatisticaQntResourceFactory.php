<?php
namespace RADAR\V1\Rest\AtividadesEstatisticaQnt;

class AtividadesEstatisticaQntResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesEstatisticaQntResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
