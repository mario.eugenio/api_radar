<?php
namespace RADAR\V1\Rest\AtividadesExcluir;

class AtividadesExcluirResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesExcluirResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
