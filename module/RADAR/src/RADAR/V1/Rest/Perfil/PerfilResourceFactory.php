<?php
namespace RADAR\V1\Rest\Perfil;

class PerfilResourceFactory
{
    public function __invoke($services)
    {
      $resource = new PerfilResource();
      $resource->setService($services->get('PerfilService'));
  
      return $resource;
    }
}
