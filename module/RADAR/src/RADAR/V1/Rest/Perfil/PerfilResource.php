<?php
namespace RADAR\V1\Rest\Perfil;

use Application\Resource\AbstractResource;
use RADAR\Service\PerfilService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class PerfilResource extends AbstractResource
{
  /** @var  PerfilService */
  public $service;
  
  /**
   * @param array $params
   * @return array|ApiProblem
   */
  public function fetchAll($params = [])
  {
    try {
      $result = $this->service->listPerfil();
      return $result;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param PerfilService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
