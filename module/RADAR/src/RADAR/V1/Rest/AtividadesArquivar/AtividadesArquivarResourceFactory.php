<?php
namespace RADAR\V1\Rest\AtividadesArquivar;

class AtividadesArquivarResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesArquivarResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
