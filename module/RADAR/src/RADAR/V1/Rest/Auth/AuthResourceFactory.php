<?php
namespace RADAR\V1\Rest\Auth;

class AuthResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AuthResource();
      $resource->setService($services->get('UsuarioService'));
      return $resource;
    }
}
