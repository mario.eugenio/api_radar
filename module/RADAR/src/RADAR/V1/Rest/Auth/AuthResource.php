<?php
namespace RADAR\V1\Rest\Auth;

use RADAR\Service\UsuarioService;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Zend\Http\Response;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AuthResource extends AbstractResourceListener
{
  /** @var  UsuarioService */
  public $service;
  
  /**
   * Create a resource
   *
   * @param  mixed $data
   * @return ApiProblem|mixed
   */
  public function create($data)
  {
    try {
      $result = $this->service->auth($data);
      
      return $result;
      
    } catch (ValidationException $ex) {
      return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
    } catch (AccessDeniedException $ex) {
      return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
    } catch (\Exception $e) {
      return new ApiProblem(401, $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param UsuarioService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
