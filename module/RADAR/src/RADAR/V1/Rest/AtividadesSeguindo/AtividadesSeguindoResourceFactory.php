<?php
namespace RADAR\V1\Rest\AtividadesSeguindo;

class AtividadesSeguindoResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesSeguindoResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
