<?php
namespace RADAR\V1\Rest\Areas;

use Application\Exception\ValidationException;
use Application\Resource\AbstractResource;
use RADAR\Service\AreaService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AreasResource extends AbstractResource
{
  /** @var  AreaService */
  public $service;
  
  /**
   * Create a resource
   *
   * @param  mixed $data
   * @return ApiProblem|mixed
   */
  public function create($data)
  {
    try {
      $result = $this->service->save($data);
      return $result;
    } catch (ValidationException $ex) {
      return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * Update a resource
   *
   * @param  mixed $id
   * @param  mixed $data
   * @return ApiProblem|mixed
   */
  public function update($id, $data)
  {
    try {
      
      $result = $this->service->save($data, $id);
      return $result;
    } catch (ValidationException $ex) {
      return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * Delete a resource
   *
   * @param mixed $id
   * @return ApiProblem|mixed
   */
  public function delete($id)
  {
    try {
      $this->service->inativarArea($id);
      return true;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param array $params
   * @return array|ApiProblem
   */
  public function fetchAll($params = [])
  {
    try {
      $result = $this->service->listAreas();
      return $result;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param AreaService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
