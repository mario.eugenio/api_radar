<?php
namespace RADAR\V1\Rest\Areas;

class AreasResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AreasResource();
      $resource->setService($services->get('AreaService'));
      return $resource;
    }
}
