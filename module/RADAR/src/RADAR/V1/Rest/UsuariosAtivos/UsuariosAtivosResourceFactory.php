<?php
namespace RADAR\V1\Rest\UsuariosAtivos;

class UsuariosAtivosResourceFactory
{
    public function __invoke($services)
    {
      $resource = new UsuariosAtivosResource();
      $resource->setService($services->get('UsuarioService'));
      return $resource;
    }
}
