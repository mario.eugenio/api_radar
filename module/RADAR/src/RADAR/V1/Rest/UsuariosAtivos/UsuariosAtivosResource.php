<?php
namespace RADAR\V1\Rest\UsuariosAtivos;

use Application\Exception\ValidationException;
use RADAR\Service\UsuarioService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class UsuariosAtivosResource extends AbstractResourceListener
{
  /** @var  UsuarioService */
  public $service;
  
  /**
   * Update a resource
   *
   * @param  mixed $id
   * @param  mixed $data
   * @return ApiProblem|mixed
   */
  public function update($id, $data)
  {
    try {
      $data = json_decode(json_encode($data), true);
  
      $result = $this->service->setAprovarUsuario($id, $data['idArea'], $data['idPerfil'], $data['aprovacao']);
      return $result;
    } catch (ValidationException $ex) {
      return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
      try {
    
        $result = $this->service->getUsuarioAtivos();
        return $result;
      } catch (\Exception $e) {
        return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
      }
    }
  
  /**
   * @param UsuarioService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
