<?php
namespace RADAR\V1\Rest\AtividadesDisponiveis;

use RADAR\Service\AtividadeService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AtividadesDisponiveisResource extends AbstractResourceListener
{
  /** @var  AtividadeService */
  public $service;
  
  /**
   * Fetch a resource
   *
   * @param  mixed $id
   * @return ApiProblem|mixed
   */
  public function fetchAll($params = [])
  {
    try {
      $arrParams = $params->toArray();
  
      if(!isset($arrParams['idUsuario'])) {
        throw new \Exception('Usuário não encontrado');
      }
      
      $result = $this->service->listarAtividadesDisponiveis($arrParams['idUsuario']);
      return $result;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param AtividadeService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
