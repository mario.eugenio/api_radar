<?php
namespace RADAR\V1\Rest\AtividadesDisponiveis;

class AtividadesDisponiveisResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesDisponiveisResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
