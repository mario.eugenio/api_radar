<?php
namespace RADAR\V1\Rest\AtividadesSeguir;

class AtividadesSeguirResourceFactory
{
    public function __invoke($services)
    {
      $resource = new AtividadesSeguirResource();
      $resource->setService($services->get('AtividadeService'));
      return $resource;
    }
}
