<?php
namespace RADAR\V1\Rest\AtividadesSeguir;

use Application\Resource\AbstractResource;
use RADAR\Service\AtividadeService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AtividadesSeguirResource extends AbstractResource
{
  /** @var  AtividadeService */
  public $service;
  
  /**
   * Update a resource
   *
   * @param  mixed $id
   * @param  mixed $data
   * @return ApiProblem|mixed
   */
  public function update($id, $data)
  {
    try {
      $result = $this->service->seguirAtividade($id, $data['idUsuario']);
      return $result;
    } catch (ValidationException $ex) {
      return new ApiProblem($ex->getCode(), $ex->getMessage(), null, htmlentities($ex->getMessage()));
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param AtividadeService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
