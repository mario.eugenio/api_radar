<?php
namespace RADAR\V1\Rest\UsuarioTime;

class UsuarioTimeResourceFactory
{
    public function __invoke($services)
    {
      $resource = new UsuarioTimeResource();
      $resource->setService($services->get('UsuarioService'));
      return $resource;
    }
}
