<?php
namespace RADAR\V1\Rest\UsuarioTime;

use Application\Resource\AbstractResource;
use RADAR\Service\UsuarioService;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class UsuarioTimeResource extends AbstractResource
{
  /** @var  UsuarioService */
  public $service;
  
  /**
   * Fetch a resource
   *
   * @param  mixed $id
   * @return ApiProblem|mixed
   */
  public function fetch($id)
  {
    try {
      $result = $this->service->getUsuarioMembrosTime($id);
      return $result;
    } catch (\Exception $e) {
      return new ApiProblem($e->getCode(), $e->getMessage(), null, htmlentities($e->getMessage()));
    }
  }
  
  /**
   * @param UsuarioService $service
   * @return $this
   */
  public function setService($service)
  {
    $this->service = $service;
    return $this;
  }
}
