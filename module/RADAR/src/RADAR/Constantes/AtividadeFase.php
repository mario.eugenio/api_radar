<?php
namespace RADAR\Constantes;

/**
 * Created by PhpStorm.
 * User: marioeugenio
 * Date: 10/26/17
 * Time: 9:19 PM
 */
class AtividadeFase
{
  const ABERTA = 1;
  const EM_ATENDIMENTO = 2;
  const CONCLUIDA = 3;
  const CANCELADA = 4;
  const ARQUIVADA = 5;
}