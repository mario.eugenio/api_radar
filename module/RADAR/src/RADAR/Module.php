<?php
namespace RADAR;

use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
          'Zend\Loader\ClassMapAutoloader' => [
            __DIR__ . '/../../autoload_classmap.php',
          ],
          'ZF\Apigility\Autoloader' => [
            'namespaces' => [
              __NAMESPACE__ => __DIR__,
            ],
          ],
        ];
    }
}
