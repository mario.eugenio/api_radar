<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */
namespace Application;

use Application\Service\AbstractService;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;
use Zend\Uri\UriFactory;

class Module
{
    public function onBootstrap(MvcEvent $event)
    {
        $app = $event->getApplication();
        $eventManager = $app->getEventManager();
        /** @var ServiceManager $srvManager */
        $srvManager = $app->getServiceManager();
        UriFactory::registerScheme('chrome-extension', 'Zend\Uri\Uri');
        AbstractService::setServiceManager($srvManager);
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        // carrega o ServiceManager para o service
       
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getViewHelperConfig()
    {
        return [
            'invokables' => [
                'version' => 'Application\View\Helper\Version',
            ]
        ];
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                ]
            ]
        ];
    }

    /**
     * Setando os Services do Componente Bee
     */
    public function getServiceConfig()
    {
        return [
            'factories' => include realpath(__DIR__ . '/../../') .'/config/php-di.config.php'
        ];
    }
}