<?php
namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;

abstract class AbstractService
{

    public static $srvManager;

    /**
     *
     * @var EntityManager
     */
    protected $defaultEntityManager;

    /**
     * Repositório de Acesso aos dados
     *
     * @var mixed
     */
    protected $repository = null;

    /**
     * Set ServiceManager
     *
     * @param ServiceManager $srvManager
     */
    public static function setServiceManager($srvManager)
    {
        static::$srvManager = $srvManager;
    }

    /**
     * Get ServiceManager
     *
     * @return ServiceManager
     */
    public static function getServiceManager()
    {
        return static::$srvManager;
    }

    /**
     *
     * @return EntityManager
     */
    public function getDefaultEntityManager()
    {
        return $this->getServiceManager()->get('doctrine.entitymanager.orm_default');
    }

    /**
     * Set repositório do Service
     *
     * @param mixed $repository
     * @return \Application\Service\AbstractService
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
        return $this;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Modifica todas as chaves de um array que possui underscore para CamelCase
     *
     * @param mixed $dados
     * @return mixed
     */
    public function underscorToCamelCase($dados)
    {
        $result = array();
        if (is_array($dados) || is_object($dados)) {
            foreach ($dados as $key => $value) {
                $arrKeyName = explode('_', $key);
                $formatedKey = '';
                for ($i = 0; $i < count($arrKeyName); $i++) {
                    if ($i == 0) {
                        $formatedKey .= strtolower($arrKeyName[$i]);
                    } else {
                        $formatedKey .= ucfirst(strtolower($arrKeyName[$i]));
                    }
                }
                $result[$formatedKey] = $value;
            }
        }
        return is_array($dados) ? $result : (object)$result;
    }

    /**
     * Passa array muldimencional para camelCase
     *
     * @param
     *            $dados
     * @return array|object
     */
    public function multiArrUnderscorToCamelCase($dados)
    {
        $result = array();
        if (is_array($dados)) {
            foreach ($dados as $v) {
                $result[] = $this->underscorToCamelCase($v);
            }
        }
        return count($result) ? $result : $dados;
    }

    /**
     * Extraido do OAUTH2
     * @param $server
     * @return array
     */
    public function getHeadersFromServer($server)
    {
        $headers = array();
        foreach ($server as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                $headers[substr($key, 5)] = $value;
            }
            // CONTENT_* are not prefixed with HTTP_
            elseif (in_array($key, array('CONTENT_LENGTH', 'CONTENT_MD5', 'CONTENT_TYPE'))) {
                $headers[$key] = $value;
            }
        }

        if (isset($server['PHP_AUTH_USER'])) {
            $headers['PHP_AUTH_USER'] = $server['PHP_AUTH_USER'];
            $headers['PHP_AUTH_PW'] = isset($server['PHP_AUTH_PW']) ? $server['PHP_AUTH_PW'] : '';
        } else {
            /*
             * php-cgi under Apache does not pass HTTP Basic user/pass to PHP by default
             * For this workaround to work, add this line to your .htaccess file:
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             *
             * A sample .htaccess file:
             * RewriteEngine On
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             * RewriteCond %{REQUEST_FILENAME} !-f
             * RewriteRule ^(.*)$ app.php [QSA,L]
             */

            $authorizationHeader = null;
            if (isset($server['HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $server['HTTP_AUTHORIZATION'];
            } elseif (isset($server['REDIRECT_HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $server['REDIRECT_HTTP_AUTHORIZATION'];
            } elseif (function_exists('apache_request_headers')) {
                $requestHeaders = (array) apache_request_headers();

                // Server-side fix for bug in old Android versions (a nice side-effect of this fix means
                // we don't care about capitalization for Authorization)
                $requestHeaders = array_combine(
                    array_map('ucwords', array_keys($requestHeaders)),
                    array_values($requestHeaders)
                );

                if (isset($requestHeaders['Authorization'])) {
                    $authorizationHeader = trim($requestHeaders['Authorization']);
                }
            }

            if (null !== $authorizationHeader) {
                $headers['AUTHORIZATION'] = $authorizationHeader;
                // Decode AUTHORIZATION header into PHP_AUTH_USER and PHP_AUTH_PW when authorization header is basic
                if (0 === stripos($authorizationHeader, 'basic')) {
                    $exploded = explode(':', base64_decode(substr($authorizationHeader, 6)));
                    if (count($exploded) == 2) {
                        list($headers['PHP_AUTH_USER'], $headers['PHP_AUTH_PW']) = $exploded;
                    }
                }
            }
        }

        // PHP_AUTH_USER/PHP_AUTH_PW
        if (isset($headers['PHP_AUTH_USER'])) {
            $headers['AUTHORIZATION'] = 'Basic '.base64_encode($headers['PHP_AUTH_USER'].':'.$headers['PHP_AUTH_PW']);
        }

        return $headers;
    }
}
