<?php
namespace Application\Service\Auth;

use Application\Service\CacheService;
use Zend\Cache\Storage\ClearExpiredInterface;
use Zend\Cache\Storage\Adapter\Filesystem;

class TokenUser
{

    private $cache;

    /**
     * Cria o token no cache
     * @param string $token
     * @param string $noLogin
     *
     * @return boolean
     */
    public function criarToken($token, $noLogin)
    {
        $cache = $this->getCache();
        // Id do cache = Token
        // Valor do cache = $noLogin
        $cache->salvaCache($token, $noLogin);
        // Verifica tokens expirados
        $this->checkExpired();
        return true;
    }

    /**
     * Verifica se já existe um arquivo para a verificação dos tokens
     */
    public function checkExpired()
    {
        $cache = $this->getCache();
        if ($cache instanceof ClearExpiredInterface) {
            $cache->clearExpired();
        }
    }

    /**
     * Apaga um registro do Cache
     *
     * @param string $token
     */
    public function deleteToken($token)
    {
        $this->getCache()->removeCache($token);
    }

    /**
     * Seta o cache para utilizar no Storage
     *
     * @param \Zend\Cache\Storage\Adapter\AbstractAdapter $cache
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
    }

    /**
     * Retorna o cache
     *
     * @return CacheService
     */
    public function getCache()
    {
        return $this->cache;
    }
}
