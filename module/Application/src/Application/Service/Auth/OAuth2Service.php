<?php

/**
 * Serviço de autenticação para controle as requisições do OAuth
 */
namespace Application\Service\Auth;

use Application\Service\CacheService;
use OAuth2\Server;

class OAuth2Service
{

    private $cache;

    /**
     *
     * @var Server
     */
    private $server;

    /**
     *
     * Realizar o login e gerar o token
     * @return array
     */
    public function login()
    {
        $server = $this->getServer();
        $requestParams = $server->getResponse()->getParameters();
        if (!isset($requestParams['error'])) {
            // trata a data de expiração
            return array(
                'noLogin' => $server->getGrantType('client_credentials')->getClientId(),
                'token' => $requestParams['access_token'],
            );
        }
        throw new \Exception('Usuário ou senha inválidos.', 401);
    }

    /**
     * Apaga um token de autenticação
     *
     * @param $token
     * @return bool
     * @throws \Exception
     */
    public function logout($token)
    {
        try {
            // verifica se o token ainda existe no cache
            if ($this->getCache()->verificaExisteCache($token)) {
                $this->getCache()->removeCache($token);
                return true;
            }
            throw new \Exception('Token não encontrado', 400);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Seta o cache para utilizar no Storage
     *
     * @param mixed $cache
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
    }

    /**
     *
     * @return CacheService
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * Seta o storage para utilizar no Storage
     *
     * @param Server $server
     */
    public function setServer($server)
    {
        $this->server = $server;
    }

    /**
     * Recupera o Server do OAuth2
     *
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }
}
