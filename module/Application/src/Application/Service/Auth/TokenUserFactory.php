<?php
/**
 * Factory do OAuth2Service para construir o objeto com as dependencias necessárias
 *
 */
namespace Application\Service\Auth;

use Interop\Container\ContainerInterface;
use Application\Service\CacheService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TokenUserFactory implements FactoryInterface
{

    /**
     * Create TokenUser
     *
     * @param ContainerInterface $container
     * @return TokenUser
     */
    public function __invoke(ContainerInterface $container)
    {
        $tokenUser = new TokenUser();
        // Cache
        $cache = new CacheService('auth_', CacheService::MEMCACHED);
        $tokenUser->setCache($cache);
        return $tokenUser;
    }

    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container);
    }
}
