<?php
/**
 * Factory do OAuth2Service para construir o objeto com as dependencias necessárias
 *
 */
namespace Application\Service\Auth;

use Interop\Container\ContainerInterface;
use Application\Service\CacheService;
use OAuth2\Request as Request;
use OAuth2\Server as Server;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class OAuth2ServiceFactory implements FactoryInterface
{

    /**
     * Create Server OAuth2
     *
     * @param ServiceLocatorInterface $container
     * @return \OAuth2\Server
     */
    public function __invoke(ContainerInterface $container)
    {
        $auth = new OAuth2Service();
        $server = $container->get('OAuth2Server');
        $server->handleTokenRequest(Request::createFromGlobals());
        $auth->setServer($server);
        // cache
        $cache = new CacheService('auth_', CacheService::MEMCACHED);
        $auth->setCache($cache);
        return $auth;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container);
    }
}
