<?php
/**
 * Factory do OAuth2Service para construir o objeto com as dependencias necessárias
 *
 */
namespace Application\Service\Auth;

use Application\Service\Auth\Storage;
use OAuth2\GrantType\ClientCredentials as ClientCredentials;
use OAuth2\Request as Request;
use OAuth2\Server;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class OAuth2ServerFactory implements FactoryInterface
{

    public function __invoke($container)
    {
        // Storage OAuth
        $storage = $container->get('OAuth2Storage');
        // Config OAuthServer
        $config = $container->get('Config');
        $server = new Server($storage, $config['oauth2']['options']);
        $server->addGrantType(new ClientCredentials($storage));
        $server->handleTokenRequest(Request::createFromGlobals());
        return $server;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container);
    }
}
