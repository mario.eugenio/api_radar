<?php
namespace Application\Service\Auth;

use Application\Service\CacheService;
use Eproinfo\Repository\UsuarioRepository;
use OAuth2\Storage\AccessTokenInterface;
use OAuth2\Storage\ClientCredentialsInterface;
use OAuth2\Storage\UserCredentialsInterface;

class Storage extends StorageAbstract implements
    AccessTokenInterface,
    UserCredentialsInterface,
    ClientCredentialsInterface
{

    const CLIENT_ID = 'client_id';
    const AUTHORIZATION = 'AUTHORIZATION';
    const PHP_AUTH_USER = 'PHP_AUTH_USER';
    const PHP_AUTH_PW = 'PHP_AUTH_PW';
    private $cache;

    /**
     * Objeto para verificar e ajustar o token
     *
     * @var TokenUser
     */
    private $tokenUser;

    /**
     * Repositório do Doctrine do para a entidade usuario
     *
     * @var UsuarioRepository
     */
    private $repositoryUsuario;

    /**
     *
     * @param $oauthToken
     * @return array
     */
    public function getAccessToken($oauthToken)
    {
        // Busca o Token no Cache
        $cache = $this->getCache();
        if (!$cache->verificaExisteCache($oauthToken)) {
            return null;
        }
        $data = $cache->retornaDadosEmCachePeloId($oauthToken);
        return array(
            static::CLIENT_ID => $data,
            'expires' => time() + 7200,
            'user_id' => $data
        );
    }

    /**
     *
     * Armazena o cache do Login
     *
     * @param \OAuth2\Storage\oauth_token $oauthToken
     * @param \OAuth2\Storage\client $clientId
     * @param \OAuth2\Storage\user $userId
     * @param int $expires
     * @param null $scope
     * @return array|Zend_Db_Statement_Interface
     * @throws Bee_Exception
     * @throws Bee_Exception_DbQueryExecution
     * @throws Exception
     */
    public function setAccessToken($oauthToken, $clientId, $userId, $expires, $scope = null)
    {
        $this->getTokenUser()->criarToken($oauthToken, $clientId, $expires);
        return true;
    }

    /**
     *
     * @param
     *            $code
     * @return array
     */
    public function getAuthorizationCode($code)
    {
        return $this->getAccessToken($code);
    }

    /**
     *
     * @param string $code
     * @param mixed $clientId
     * @param mixed $userId
     * @param string $redirect_uri
     * @param int $expires
     * @param null $scope
     */
    public function setAuthorizationCode($code, $clientId, $userId, $redirect_uri, $expires, $scope = null)
    {
        $this->setAccessToken($code, $clientId, $userId, $expires, $scope);
    }

    /**
     * Verifica os dados do Usuario
     *
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public function checkUserCredentials($noLogin, $password)
    {
        $result = false;
        $repository = $this->getRepository();
        $usuario = $repository->dadosUsuarioPeloLogin($noLogin);

        $password = $this->encryptPass($password);
        if ($usuario['ds_senha'] === $password) {
            $result = true;
        }
        return $result;
    }

    /**
     * Recupera a Inscrição do ENEM
     *
     * @param
     *            $username
     * @return array
     */
    public function getUserDetails($noLogin)
    {
        $objUsuario = $this->getRepository()->dadosUsuarioPeloLogin($noLogin);
        return array(
            static::CLIENT_ID => $objUsuario['no_login']
        );
    }

    public function getClientDetails($clientId)
    {
        return $this->getUserDetails($clientId);
    }

    public function getClientScope($clientId)
    {
        return null;
    }

    /**
     * Verifica o tipo de Grant Permitido
     *
     * @see \OAuth2\Storage\ClientInterface::checkRestrictedGrantType()
     */
    public function checkRestrictedGrantType($clientId, $grant_type)
    {
        return ($grant_type == 'client_credentials');
    }

    /**
     * Seta o cache para utilizar no Storage
     *
     * @param \Zend\Cache\Storage\Adapter\AbstractAdapter $cache
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
    }

    /**
     * Retorna o cache
     *
     * @return CacheService
     */
    public function getCache()
    {
        return $this->cache;
    }

    public function setRepository($repository)
    {
        $this->repositoryUsuario = $repository;
    }

    /**
     *
     * @return \Eproinfo\Repository\UsuarioRepository
     */
    public function getRepository()
    {
        return $this->repositoryUsuario;
    }

    /**
     *
     * @return \Application\Service\Auth\TokenUserFactory
     */
    public function getTokenUser()
    {
        return $this->tokenUser;
    }

    public function setTokenUser($tokenUser)
    {
        $this->tokenUser = $tokenUser;
        return $this;
    }

    /**
     * @param $clientId
     * @param null $client_secret
     * @return bool
     */
    public function checkClientCredentials($clientId, $client_secret = null)
    {
        return $this->checkUserCredentials($clientId, $client_secret);
    }

    /**
     * Recupera os dados do usuário através do TOKEN enviado
     * @return null
     */
    public function getUserToken()
    {
        $tokenHttp = $this->getHeadersFromServer($_SERVER);
        $tokenHttp = $tokenHttp[static::AUTHORIZATION];
        $result = null;
        if (isset($tokenHttp) && !empty($tokenHttp)) {
            $arrToken = explode(" ", $tokenHttp);
            $token = end($arrToken);
            $dadosToken = $this->getAccessToken($token);

            if (!empty($dadosToken[static::CLIENT_ID])) {
                $result = $this->repositoryUsuario->dadosUsuarioPeloLogin($dadosToken[static::CLIENT_ID]);
            }
        }

        return $result;

    }

    public function isPublicClient($clientId)
    {
    }

    /**
     * Extraido do OAUTH2
     * @param $server
     * @return array
     */
    public function getHeadersFromServer($server)
    {
        $headers = array();
        foreach ($server as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                $headers[substr($key, 5)] = $value;
            }
            // CONTENT_* are not prefixed with HTTP_
            elseif (in_array($key, array('CONTENT_LENGTH', 'CONTENT_MD5', 'CONTENT_TYPE'))) {
                $headers[$key] = $value;
            }
        }

        if (isset($server[static::PHP_AUTH_USER])) {
            $headers[static::PHP_AUTH_USER] = $server[static::PHP_AUTH_USER];
            $headers[static::PHP_AUTH_USER] = isset($server[static::PHP_AUTH_USER]) ?
                $server[static::PHP_AUTH_USER] : '';
        } else {
            /*
             * php-cgi under Apache does not pass HTTP Basic user/pass to PHP by default
             * For this workaround to work, add this line to your .htaccess file:
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             *
             * A sample .htaccess file:
             * RewriteEngine On
             * RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
             * RewriteCond %{REQUEST_FILENAME} !-f
             * RewriteRule ^(.*)$ app.php [QSA,L]
             */

            $authorizationHeader = null;
            if (isset($server['HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $server['HTTP_AUTHORIZATION'];
            } elseif (isset($server['REDIRECT_HTTP_AUTHORIZATION'])) {
                $authorizationHeader = $server['REDIRECT_HTTP_AUTHORIZATION'];
            } elseif (function_exists('apache_request_headers')) {
                $requestHeaders = (array) apache_request_headers();

                // Server-side fix for bug in old Android versions (a nice side-effect of this fix means
                // we don't care about capitalization for Authorization)
                $requestHeaders = array_combine(
                    array_map('ucwords', array_keys($requestHeaders)),
                    array_values($requestHeaders)
                );

                if (isset($requestHeaders[static::AUTHORIZATION])) {
                    $authorizationHeader = trim($requestHeaders[static::AUTHORIZATION]);
                }
            }

            if (null !== $authorizationHeader) {
                $headers[static::AUTHORIZATION] = $authorizationHeader;
                // Decode AUTHORIZATION header into PHP_AUTH_USER and PHP_AUTH_PW when authorization header is basic
                if (0 === stripos($authorizationHeader, 'basic')) {
                    $exploded = explode(':', base64_decode(substr($authorizationHeader, 6)));
                    if (count($exploded) == 2) {
                        list($headers[static::PHP_AUTH_USER], $headers[static::PHP_AUTH_USER]) = $exploded;
                    }
                }
            }
        }

        // PHP_AUTH_USER/PHP_AUTH_PW
        if (isset($headers[static::PHP_AUTH_USER])) {
            $headers[static::AUTHORIZATION] = 'Basic '.
                base64_encode($headers[static::PHP_AUTH_USER].':'.$headers[static::PHP_AUTH_USER]);
        }

        if (!isset($headers[static::AUTHORIZATION])) {
            $headers =  getallheaders();
            $authorizationHeader = isset($headers["Authorization"]) ? $headers["Authorization"] : null;
            $authorizationHeader = $authorizationHeader?: isset($headers[static::AUTHORIZATION]);
            $authorizationHeader = $authorizationHeader?: isset($headers[strtolower(static::AUTHORIZATION)]);
            $headers[static::AUTHORIZATION] = $authorizationHeader;
        }
        return $headers;
    }

    private function encryptPass($strSenha)
    {
        return base64_encode(hash('sha256', $strSenha,true));
    }
}
