<?php
namespace Application\Service\Auth;

use OAuth2\Storage\ClientCredentialsInterface;

class StorageBase extends StorageAbstract implements ClientCredentialsInterface
{

    public function checkClientCredentials($client_id, $client_secret = null)
    {
        return $this->checkUserCredentials($client_id, $client_secret);
    }

    public function isPublicClient($client_id)
    {
        return false;
    }
}
