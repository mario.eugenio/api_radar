<?php
/**
 * Factory do OAuth2Service para construir o objeto com as dependencias necessárias
 *
 */
namespace Application\Service\Auth;

use Application\Service\Auth\Storage;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Application\Service\CacheService;
use OAuth2\Server as Server;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class OAuth2StorageFactory implements FactoryInterface
{

    /**
     * Create Server OAuth2
     *
     * @param ContainerInterface $container
     * @return \OAuth2\Server
     */
    public function __invoke(ContainerInterface $container)
    {
        $storage = new Storage();
        // Token User - Geração de Cache
        $tokenUser = $container->get('TokenUser');
        $storage->setTokenUser($tokenUser);
        // Cache
        $cache = new CacheService('auth_', CacheService::MEMCACHED);
        $storage->setCache($cache);
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $repository = $entityManager->getRepository('Eproinfo\Entity\Usuario');
        $storage->setRepository($repository);
        return $storage;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container);
    }
}
