<?php
namespace Application\Service\Auth;

use OAuth2\Storage\JwtBearerInterface;
use OAuth2\Storage\RefreshTokenInterface;
use OAuth2\Storage\ScopeInterface;
use OAuth2\Storage\ClientCredentialsInterface;

class StorageAbstract implements RefreshTokenInterface, JwtBearerInterface, ScopeInterface, ClientCredentialsInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\RefreshTokenInterface::getRefreshToken()
     */
    public function getRefreshToken($refresh_token)
    {
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\RefreshTokenInterface::setRefreshToken()
     */
    public function setRefreshToken($refresh_token, $client_id, $user_id, $expires, $scope = null)
    {
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\RefreshTokenInterface::unsetRefreshToken()
     */
    public function unsetRefreshToken($refresh_token)
    {
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\JwtBearerInterface::getClientKey()
     */
    public function getClientKey($client_id, $subject)
    {
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\JwtBearerInterface::getJti()
     */
    public function getJti($client_id, $subject, $audience, $expiration, $jti)
    {
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\JwtBearerInterface::setJti()
     */
    public function setJti($client_id, $subject, $audience, $expiration, $jti)
    {
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\ScopeInterface::scopeExists()
     */
    public function scopeExists($scope)
    {
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\ScopeInterface::getDefaultScope()
     */
    public function getDefaultScope($client_id = null)
    {
        return null;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\ClientCredentialsInterface::checkClientCredentials()
     */
    public function checkClientCredentials($client_id, $client_secret = null)
    {
        return true;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \OAuth2\Storage\ClientCredentialsInterface::isPublicClient()
     */
    public function isPublicClient($client_id)
    {
        return true;
    }

    public function getClientDetails($client_id)
    {
        return true;
    }

    /**
     * Get the scope associated with this client
     *
     * @return STRING the space-delineated scope list for the specified client_id
     */
    public function getClientScope($client_id)
    {
        return true;
    }

    /**
     * Check restricted grant types of corresponding client identifier.
     *
     * If you want to restrict clients to certain grant types, override this
     * function.
     *
     * @param $client_id Client
     *            identifier to be check with.
     * @param $grant_type Grant
     *            type to be check with
     *
     * @return TRUE if the grant type is supported by this client identifier, and
     *         FALSE if it isn't.
     *
     *         @ingroup oauth2_section_4
     */
    public function checkRestrictedGrantType($client_id, $grant_type)
    {
        return true;
    }
}
