<?php
namespace Application\Service;

use Doctrine\Common\Cache\CacheProvider;

class CacheService extends AbstractService
{

    const MEMCACHED = 'MemcachedCache';

    const ZEND_DATA_CACHE = 'ZendDataCache';

    /** @var  CacheProvider */
    private $cache;

    /**
     * @var string
     */
    private $classNamespace;

    /**
     * @var string
     */
    private $reposotiry;

    /**
     * @param $classNamespace
     * @param null $typeCache
     */
    public function __construct($classNamespace, $typeCache = null)
    {
        if ($typeCache === null) {
            $typeCache = self::ZEND_DATA_CACHE;
        }
        $this->setCache($this->retornaCache($typeCache));
        $this->classNamespace = $classNamespace;
    }

    /**
     * @param $idCache
     * @return bool
     */
    public function verificaExisteCache($idCache)
    {
        return $this->getCache()->hasItem($this->getKey($idCache));
    }

    /**
     * @param $idCache
     * @param $dados
     * @return false|mixed
     */
    public function salvaCache($idCache, $dados)
    {
        $this->getCache()->setItem($this->getKey($idCache), $dados);
        return $this->retornaDadosEmCachePeloId($idCache);
    }

    /**
     * @param $idCache
     * @param $dados
     * @return false|mixed
     */
    public function salvaCacheSeNaoExistir($idCache, $dados)
    {
        if (!$this->verificaExisteCache($idCache)) {
            $this->getCache()->setItem($this->getKey($idCache), $dados);
        }
        return $this->retornaDadosEmCachePeloId($idCache);
    }

    /**
     * @param $idCache
     */
    public function removeCache($idCache)
    {
        if ($this->verificaExisteCache($idCache)) {
            $this->getCache()->removeItem($this->getKey($idCache));
        }
    }

    /**
     * @param $idCache
     * @return false|mixed
     */
    public function retornaDadosEmCachePeloId($idCache)
    {
        return $this->getCache()->getItem($this->getKey($idCache));
    }

    public function getKey($key)
    {
        return $this->classNamespace . $key;
    }

    public function getCache()
    {
        return $this->cache;
    }

    public function setCache($cache)
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @param $typeCache
     * @return null
     * @throws \Exception
     */
    private function retornaCache($typeCache)
    {
        $this->verificaCacheValido($typeCache);

        $cacheAdapter = null;
        if ($typeCache === self::MEMCACHED) {
            $cacheAdapter = $this->getServiceManager()->get('memcached');
        }

        if ($typeCache === self::ZEND_DATA_CACHE) {
            $cacheAdapter = $this->getServiceManager()->get('zendservershm');
        }

        if (empty($cacheAdapter)) {
            throw new \RuntimeException('Não foi possível recuperar o CacheAdapter do Service Manager');
        }

        return $cacheAdapter;
    }

    /**
     * Limpa dados do Cache
     */
    public function limpaCache()
    {
        $this->getCache()->flush();
    }

    /**
     * Verifica se cache é avlido
     *
     * @param $typeCache
     * @return bool
     * @throws \Exception
     */
    private function verificaCacheValido($typeCache)
    {
        $validCaches = array(
            self::MEMCACHED,
            self::ZEND_DATA_CACHE
        );
        if (!in_array($typeCache, $validCaches)) {
            throw new \Exception('Cache não suportado pela aplicação.');
        }
        return true;
    }

    /**
     * Definir qual a classe que deve ser implementada para o repositório
     *
     * @return string
     */
    protected function getTypeRepository()
    {
        return $this->reposotiry;
    }

    /**
     * Definir qual a classe que deve ser implementada para o repositório
     *
     * @param $reposotiry
     */
    protected function setTypeRepository($reposotiry)
    {
        $this->reposotiry = $reposotiry;
    }
}