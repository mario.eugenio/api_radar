<?php

abstract class AbstractEntity
{
  public function __construct(array $data = null)
  {
    $this->setData($data);
  }
  
  public  function setData($data)
  {
    foreach ((array) $data as $key => $value) {
      if (substr(ucfirst ($key), 0, 4) != 'List') {
        $set = "set" . ucfirst ($key);
        
        if (method_exists($this, $set)) {
          if (strtolower(substr($key, 0,4)) == 'data'
            || strtolower(substr($key, 0,2)) == 'dt') {
            $value = new \DateTime($value);
          }
          
          $this->$set($value);
        }
      }
    }
  }
  
  /**
   * Converte entidade para array
   *
   * @return array
   */
  public function toArray()
  {
    $object = $this;
    $reflectionClass = new ReflectionClass(get_class($object));
    $array = array();
    foreach ($reflectionClass->getProperties() as $property) {
      $property->setAccessible(true);
      $array[$property->getName()] = $property->getValue($object);
      $property->setAccessible(false);
    }
    return $array;
  }
}