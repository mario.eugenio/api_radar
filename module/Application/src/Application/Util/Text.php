<?php

/**
 *
 * Utilitário para Textos em Geral
 *
 */
namespace Application\Util;

use Zend\Filter\StringToLower;
use Zend\I18n\Filter\Alpha;

class Text
{

    /**
     * Formata a documentação
     *
     * @param string $documentacao
     * @return string
     */
    public static function formataDocumentacao($documentacao)
    {
        if (! empty($documentacao)) {
            return nl2br(trim($documentacao));
        }
        return "";
    }

    /**
     * @link http://codigofonte.uol.com.br/codigos/formatacao-de-nomes-proprios-em-php
     * @param string $nome
     * @return string
     */
    public static function tratarNome($nome)
    {
        // Separa o nome por espaços
        $nome = explode(" ", mb_strtolower($nome));
        $saida = null;
        for ($i=0; $i < count($nome); $i++) {
            $nomeTratamento = array("de", "da", "e", "dos","do");
            // Tratar cada palavra do nome
            if (in_array($nome[$i], $nomeTratamento)) {
                // Se a palavra estiver dentro das complementares mostrar toda em minúsculo
                $saida .= $nome[$i].' ';
            } else {
                // Se for um nome, mostrar a primeira letra maiúscula
                $saida .= ucfirst($nome[$i]).' ';
            }

        }
        return trim($saida);
    }

    public static function tratarTextoBusca($txt)
    {
        $txt = strip_tags($txt);
        return trim($txt);
    }

    public static function ajustarChaveCache($chave)
    {
        $filterString = new Alpha();
        $filterLower = new StringToLower();
        $txt = $filterLower->filter($chave);
        return $filterString->filter($txt);
    }

    public static function criaChaveCache(array $params, $class)
    {
        $strCache = $class . '';
        $arrReplace = array(
            ',' => '_._',
            ' ' => '_',
        );
        foreach ($params as $key => $value) {
            $strCache .= str_replace(array_keys($arrReplace), array_values($arrReplace), strip_tags($key)) .
                str_replace(array_keys($arrReplace), array_values($arrReplace), strip_tags($value));
        }
        return $strCache;
    }

    /**
     * @param $string
     * @return mixed
     */
    public static function removeAcentos($string)
    {
        return preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
    }
}
