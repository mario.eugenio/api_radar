<?php

/**
 *
 * Utilitário para Datas
 */
namespace Application\Util;

class Date
{

    /**
     * Converte uma data para um formato específico
     *
     * @param string $data
     * @param string $formato
     * @return string
     */
    public static function formataData($data = null, $formato = null)
    {
        $ts = $data ? strtotime($data) : time();
        if (! $formato) {
            $dia = date('d/m/Y', $ts);
            $horario = date('G\hi', $ts);
            $singular = '0h00' == $horario || '1h00' == $horario;
            $data = $dia . ($singular ? ' à ' : ' às ') . $horario;
        } elseif ('F' == $formato) {
                $data = strftime('%B', $ts);
            } else {
                $data = date($formato, $ts);
            }
        if ('00' == substr($data, - 2)) {
            $data = substr($data, 0, - 2);
        }
        return $data;
    }

    /**
     * Recupera o Datetime com os milissegundos para utilizar no Mobile
     *
     * @param \DateTime $date
     * @return number
     */
    public static function getTimestampMobile($date)
    {
        return $date->getTimestamp() * 1000;
    }

    /**
     * Formatação de dados para o SISU no formato mais legível
     * para a classificação parcial e para o boletim do aluno
     *
     * @param string $data
     * @param string $formato
     * @return string
     */
    public static function data($data = null, $formato = null)
    {
        $ts = $data ? strtotime($data) : time();
        if (! $formato) {
            $dia = date('d/m/Y', $ts);
            $horario = date('G\hi', $ts);
            $singular = '0h00' == $horario || '1h00' == $horario;
            $data = $dia . ($singular ? ' à ' : ' às ') . $horario;
        } elseif ('F' == $formato) {
                $data = strftime('%B', $ts);
            } else {
                $data = date($formato, $ts);
            }
        if ('00' == substr($data, - 2)) {
            $data = substr($data, 0, - 2);
        }
        return $data;
    }
}
