<?php
$excludeDirectories = array(
    '.',
    '..',
    '.svn'
);
$arrWindows = array(
    'WIN32',
    'WINNT',
    'Windows'
);
$directories = scandir(realpath(__DIR__ . '/../../../../../module'));

foreach ($directories as $dir) {
    if (! in_array($dir, $excludeDirectories)) {
        if (in_array(PHP_OS, $arrWindows)) {
            exec('"' . realpath(__DIR__ . '/../../../../../vendor/bin/') . DIRECTORY_SEPARATOR .
                'classmap_generator.php.bat"' . ' -l "' . realpath(__DIR__ . '/../../../../../module/') .
                DIRECTORY_SEPARATOR . $dir . '"');
        } else {
            exec('php ' . realpath(__DIR__ . '/../../../../../vendor/bin/') . DIRECTORY_SEPARATOR .
                'classmap_generator.php' . ' -l ' . realpath(__DIR__ . '/../../../../../module/') .
                DIRECTORY_SEPARATOR . $dir);
        }
        echo "\n -- Gerando Class Mapping '" . $dir . "'";
    }
}
echo "\n";

