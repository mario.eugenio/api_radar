<?php

/**
 * Classe para abstrair problemas da aplicação
 * e armazenar os Custom Events do Zend Server
 */
namespace Application\Exception;

use ZF\ApiProblem\ApiProblem;

class Problem extends ApiProblem
{

    public function __construct($status, $detail = null, $type = null, $title = null, array $additional = [])
    {
        $details = array();
        // Verifica se é uma Exception
        if ($status instanceof \Exception) {
            // Evento Zend Server
            $exception = $status;

            $details = array(
                'Arquivo gerador da Exception' => $exception->getFile(),
                'Linha' => $exception->getLine(),
                'Mensagem' => utf8_encode($exception->getMessage()),
                'Codigo da Exception' => $exception->getCode(),
                'Exception anterior' => utf8_encode($exception->getPrevious()),
                'Trace' => $exception->getTraceAsString()
            );
            $detailEvent = $status->getCode() . " - " . $status->getMessage();
            $detail = $status->getMessage();
            $status = $status->getCode();
        }
        // Cria o Custom Event do Zend Server
        if (function_exists('zend_monitor_custom_event')) {
            zend_monitor_custom_event($status, $detailEvent, $details);
        }

        parent::__construct($status, $detail, $type, $title, $additional);
    }
}
